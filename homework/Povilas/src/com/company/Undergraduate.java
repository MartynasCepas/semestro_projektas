package com.company;

class Undergraduate extends Student
{
    public Undergraduate(String name, String surname, String degree, String science, Double average)
    {
        this.Name = name;
        this.Surname = surname;
        this.Degree = degree;
        this.Science = science;
        this.Average = average;
        this.Scholarship = Scholarship();
    }

    public int Scholarship()
    {
        if (Average >= 8)
        {
            return 100;
        }
        return 0;
    }

    public String toString()
    {
        return Name + " " + Surname + " will get " + Scholarship + " EUR scholarship";
    }
}