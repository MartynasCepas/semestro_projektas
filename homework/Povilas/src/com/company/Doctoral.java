package com.company;

class Doctoral extends Student
{
    public Doctoral(String name, String surname, String degree, String science, Double average)
    {
        this.Name = name;
        this.Surname = surname;
        this.Degree = degree;
        this.Science = science;
        this.Average = average;
        this.Scholarship = Scholarship();
    }

    public int Scholarship()
    {
        return 800;
    }

    public String toString()
    {
        return "Dr. " + Name + " " + Surname + " will get " + Scholarship + " EUR scholarship";
    }
}
