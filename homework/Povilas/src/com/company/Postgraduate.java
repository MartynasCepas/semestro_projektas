package com.company;

class Postgraduate extends Student
{
    public Postgraduate(String name, String surname, String degree, String science, Double average)
    {
        this.Name = name;
        this.Surname = surname;
        this.Degree = degree;
        this.Science = science;
        this.Average = average;
        this.Scholarship = Scholarship();
    }

    public int Scholarship()
    {
        if (Science.equalsIgnoreCase("mathematics") || Science.equalsIgnoreCase("physics"))
        {
            return 200;
        }
        return 0;
    }

    public String toString()
    {
        return Name + " " + Surname + " will get " + Scholarship + " EUR scholarship";
    }
}


