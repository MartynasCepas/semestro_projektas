package com.company;

public class StudentFactory
{
    public static Student getStudent(String name, String surname, String degree, String science, Double average)
    {
        if (degree.equalsIgnoreCase("Undergraduate")) {
            return new Undergraduate(name, surname, degree, science, average);
        }
        if (degree.equalsIgnoreCase("Postgraduate")) {
            return new Postgraduate(name, surname, degree, science, average);
        }
        if (degree.equalsIgnoreCase("Doctoral")) {
            return new Doctoral(name, surname, degree, science, average);
        }
        throw new IllegalArgumentException("Unsupported type " + degree);
    }
}
