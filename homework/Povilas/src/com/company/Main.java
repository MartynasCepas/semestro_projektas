package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class Main {

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("${name} ${surname} ${undergraduate|postgraduate|doctoral} ${science} ${average}");

        while (true)
        {
            String line = reader.readLine();
            String[] info = line.split(" ");
            System.out.println(StudentFactory.getStudent(info[0], info[1], info[2], info[3], Double.parseDouble(info[4])).toString());
        }
    }
}

