public class Postgraduate extends Student {

    public Postgraduate(String firstName, String lastName, String studyLevel, String programme, double gradesAverage) {
        super(firstName, lastName, studyLevel, programme, gradesAverage);
        setScholarship();
        setFullName();
    }

    @Override
    protected void setFullName() {
        fullName = firstName + " " + lastName;
    }

    @Override
    protected void setScholarship() {
        if(programme.equalsIgnoreCase("Mathematics") || programme.equalsIgnoreCase("Physics"))
            scholarship = 200;
        else
            scholarship = 0;
    }
}
