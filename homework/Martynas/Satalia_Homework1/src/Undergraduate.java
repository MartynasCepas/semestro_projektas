public class Undergraduate extends Student{

    public Undergraduate(String firstName, String lastName, String studyLevel, String programme, double gradesAverage) {
        super(firstName, lastName, studyLevel, programme, gradesAverage);
        setScholarship();
        setFullName();
    }

    @Override
    protected void setFullName() {
        fullName = firstName + " " + lastName;
    }

    @Override
    protected void setScholarship() {
        if(this.gradesAverage > 8){
            scholarship = 100;
        }
    }
}
