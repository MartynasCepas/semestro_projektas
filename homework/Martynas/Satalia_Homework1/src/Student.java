public abstract class Student {
    protected String fullName;
    protected final String firstName;
    protected final String lastName;
    protected final String studyLevel;
    protected final String programme;
    protected final double gradesAverage;
    protected int scholarship;

    public Student(String firstName, String lastName, String studyLevel, String programme, double gradesAverage){
        this.firstName = firstName;
        this.lastName = lastName;
        this.studyLevel = studyLevel;
        this.programme = programme;
        this.gradesAverage = gradesAverage;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getStudyLevel(){
        return studyLevel;
    }

    public String getProgramme(){
        return programme;
    }

    public Double getGradesAverage(){
        return gradesAverage;
    }

    public int getScholarship(){
        return scholarship;
    }

    protected abstract void setFullName();

    public String getFullName(){
        return fullName;
    }

    protected abstract void setScholarship();

    public void output(){
        System.out.println(new String(fullName + " will get " + scholarship + "$ scholarship"));
    }
}
