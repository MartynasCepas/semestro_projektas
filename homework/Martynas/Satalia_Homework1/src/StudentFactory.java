public class StudentFactory {
    public static Student getStudent(String firstName, String lastName, String studyLevel, String programme, double gradesAverage){
        if(studyLevel.equalsIgnoreCase("Undergraduate"))
            return new Undergraduate(firstName, lastName, studyLevel, programme, gradesAverage);
        if(studyLevel.equalsIgnoreCase("Postgraduate"))
            return new Postgraduate(firstName, lastName, studyLevel, programme, gradesAverage);
        if(studyLevel.equalsIgnoreCase("PHD"))
            return new PhD(firstName, lastName, studyLevel, programme, gradesAverage);
        throw new IllegalArgumentException("Unsupported study level " + studyLevel);
    }
}
