public class PhD extends Student{

    public PhD(String firstName, String lastName, String studyLevel, String programme, double gradesAverage) {
        super(firstName, lastName, studyLevel, programme, gradesAverage);
        setScholarship();
        setFullName();
    }

    @Override
    protected void setFullName() {
        fullName = "Dr. " + firstName + " " + lastName;
    }

    @Override
    protected void setScholarship() {
        scholarship = 800;
    }
}
