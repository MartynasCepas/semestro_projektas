import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.println("Type in student information as following:");
        System.out.println("${firstName} ${lastName} ${Undergraduate|Postgraduate|PHD} ${programme} ${average}");
        Scanner in = new Scanner(System.in);
        while(true){
            var input = in.nextLine();
            var variables = input.split(" ");
            StudentFactory.getStudent(variables[0], variables[1], variables[2], variables[3], Double.parseDouble(variables[4])).output();
        }
    }
}
