public class Bachelor extends Student {
    public Bachelor(String firstName, String lastName, String study, String programme, double gradesAverage) {
        super(firstName, lastName, study, programme, gradesAverage);
        this.setScholarship();
        this.setFullName();
    }

    protected void setFullName() {
        this.fullName = this.firstName + " " + this.lastName;
    }

    protected void setScholarship() {
        if (this.gradesAverage > 8) {
            this.scholarship = 100;
        }

    }
}
