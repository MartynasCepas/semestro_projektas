public class Doctor extends Student {
    public Doctor(String firstName, String lastName, String study, String programme, double gradesAverage) {
        super(firstName, lastName, study, programme, gradesAverage);
        this.setScholarship();
        this.setFullName();
    }

    protected void setFullName() {
        this.fullName = "Dr. " + this.firstName + " " + this.lastName;
    }

    protected void setScholarship() {
        this.scholarship = 800;
    }
}