public class Student_factory {
    public Student_factory() {
    }

    public static Student getStudent(String firstName, String lastName, String studyLevel, String programme, double gradesAverage) {
        if (studyLevel.equalsIgnoreCase("Bakalauras")) {
            return new Bachelor(firstName, lastName, studyLevel, programme, gradesAverage);
        } else if (studyLevel.equalsIgnoreCase("Magistras")) {
            return new Master(firstName, lastName, studyLevel, programme, gradesAverage);
        } else if (studyLevel.equalsIgnoreCase("Docentas")) {
            return new Doctor(firstName, lastName, studyLevel, programme, gradesAverage);
        } else {
            throw new IllegalArgumentException("Tokiu studiju nera " + studyLevel);
        }
    }
}