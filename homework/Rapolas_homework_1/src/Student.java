public abstract class Student {
    protected String fullName;
    protected String firstName;
    protected String lastName;
    protected String study;
    protected String programme;
    protected double gradesAverage;
    protected int scholarship;

    public Student(String firstName, String lastName, String study, String programme, double gradesAverage) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.study = study;
        this.programme = programme;
        this.gradesAverage = gradesAverage;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getStudyLevel() {
        return this.study;
    }

    public String getProgramme() {
        return this.programme;
    }

    public Double getGradesAverage() {
        return this.gradesAverage;
    }

    public int getScholarship() {
        return this.scholarship;
    }

    protected abstract void setFullName();

    public String getFullName() {
        return this.fullName;
    }

    protected abstract void setScholarship();

    public void print() {
        System.out.println(new String(this.fullName + " gaus " + this.scholarship + "stipendija"));
    }
}