public class Master extends Student {
    public Master(String firstName, String lastName, String study, String programme, double gradesAverage) {
        super(firstName, lastName, study, programme, gradesAverage);
        this.setScholarship();
        this.setFullName();
    }

    protected void setFullName() {
        this.fullName = this.firstName + " " + this.lastName;
    }

    protected void setScholarship() {
        if (!this.programme.equalsIgnoreCase("Matematika") && !this.programme.equalsIgnoreCase("Fizika")) {
            this.scholarship = 0;
        } else {
            this.scholarship = 200;
        }

    }
}