import java.util.Scanner;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        System.out.println("Irasykite studento duomenis:");
        System.out.println("${vardas} ${pavarde} ${Bakalauras|Magistras|Docentas} ${studiju programa} ${vidurkis}");
        Scanner in = new Scanner(System.in);

        while(true) {
            String input = in.nextLine();
            String[] parts = input.split(" ");
            Student_factory.getStudent(parts[0], parts[1], parts[2], parts[3], Double.parseDouble(parts[4])).print();
        }
    }
}
