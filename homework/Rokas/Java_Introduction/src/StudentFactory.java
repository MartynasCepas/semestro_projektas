public class StudentFactory {
    public static Student getStudent(String name, String lastName, String degree, String studyField, Double average)
    {
        if (degree.equalsIgnoreCase("Bachelor")) {
            return new Bachelor(name, lastName, degree, studyField, average);
        }
        if (degree.equalsIgnoreCase("Master")) {
            return new Master(name, lastName, degree, studyField, average);
        }
        if (degree.equalsIgnoreCase("Doctor")) {
            return new Doctor(name, lastName, degree, studyField, average);
        }
        throw new IllegalArgumentException("Bad degree type " + degree);
    }
}
