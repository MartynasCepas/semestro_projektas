public abstract class Student {

    protected String Name;
    protected String LastName;
    protected String Degree;
    protected String StudyField;
    protected double Average;
    protected int scholarship;

    public Student(){}

    public Student(String name, String lastName, String Degree, String studyField, double average){}

    protected abstract void setScholarship();

    public abstract String toString();

    public String getName() {
        return Name;
    }

    public String getLastName() {
        return LastName;
    }

    public String getDegree() {
        return Degree;
    }

    public String getStudyField() {
        return StudyField;
    }

    public double getAverage() {
        return Average;
    }
}
