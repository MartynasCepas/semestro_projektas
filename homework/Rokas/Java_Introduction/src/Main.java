import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {
    public static void main(String args[])throws IOException {
        System.out.println("Please write information like this:");
        System.out.println("${firstName} ${lastName} ${Bachelor|Master|Doctor} ${programme} ${average}");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true){
            String input = reader.readLine();
            String[] var = input.split(" ");
            System.out.println(StudentFactory.getStudent(var[0], var[1], var[2], var[3], Double.parseDouble(var[4])).toString());
        }

    }
}
