public class Doctor extends Student {

    public Doctor(String name, String lastName, String degree,String studyField, double average){
        Name = name;
        LastName = lastName;
        Degree = degree;
        StudyField = studyField;
        Average = average;
        setScholarship();
    }
    @Override
    protected void setScholarship(){
            scholarship = 800;
    }
    @Override
    public String toString() {
        return "Dr. " + Name + " " + LastName + " will get " + scholarship + "EUR scholarship" ;
    }
}
