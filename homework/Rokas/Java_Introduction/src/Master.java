public class Master extends Student {

    public Master(String name, String lastName, String degree, String studyField, double average){
        Name = name;
        LastName = lastName;
        Degree = degree;
        StudyField = studyField;
        Average = average;
        setScholarship();
    }
    @Override
    protected void setScholarship(){
        if(StudyField.equalsIgnoreCase("Chemics") || StudyField.equalsIgnoreCase("Mathematichs")|| StudyField.equalsIgnoreCase("IT"))
            scholarship = 200;
        else
            scholarship = 0;
    }
    @Override
    public String toString() {
        return Name + " " + LastName + " will get " + scholarship + "EUR scholarship" ;
    }
}
