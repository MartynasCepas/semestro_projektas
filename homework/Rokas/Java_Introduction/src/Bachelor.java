public class Bachelor extends Student {

    public Bachelor(String name, String lastName, String degree, String studyField, double average){
        Name = name;
        LastName = lastName;
        Degree = degree;
        StudyField = studyField;
        Average = average;
        setScholarship();
    }

    @Override
    protected void setScholarship(){
        if(this.Average > 8){
            scholarship = 100;
        }
    }

    @Override
    public String toString() {
        return Name + " " + LastName + " will get " + scholarship + "EUR scholarship" ;
    }

}
