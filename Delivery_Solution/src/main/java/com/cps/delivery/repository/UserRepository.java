package com.cps.delivery.repository;

import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String email);

    User findByEmailAndPassword(String email, String password);

    User findById(int id);

    @Override
    Iterable<User> findAll();
}
