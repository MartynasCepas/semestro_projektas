package com.cps.delivery.repository;

import com.cps.delivery.domain.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends CrudRepository<Admin, Integer> {

    Admin findById(int id);
}
