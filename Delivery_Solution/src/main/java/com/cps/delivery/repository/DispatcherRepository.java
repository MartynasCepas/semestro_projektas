package com.cps.delivery.repository;

import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.domain.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

import java.util.List;

@Repository
public interface DispatcherRepository extends CrudRepository<Dispatcher, Integer> {

    @Override
    Iterable<Dispatcher> findAll();

    Dispatcher findById(int id);

    Dispatcher findDispatcherByEmailAndPassword(String email, String password);

    Dispatcher findDispatcherByEmail(String email);
}
