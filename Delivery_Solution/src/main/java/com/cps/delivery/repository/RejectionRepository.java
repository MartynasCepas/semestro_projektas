package com.cps.delivery.repository;

import com.cps.delivery.domain.Rejection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RejectionRepository extends CrudRepository<Rejection, Integer> {

    @Override
    Iterable<Rejection> findAll();

    List<Rejection> findRejectionsByRejectionsForCourierId(int courierId);
}
