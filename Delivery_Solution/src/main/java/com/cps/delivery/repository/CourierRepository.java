package com.cps.delivery.repository;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Rejection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourierRepository extends CrudRepository<Courier, Integer> {

    @Override
    Iterable<Courier> findAll();

    Courier findById(int courierId);

    Iterable<Rejection> findCourierById(int courierId);

    Courier findCourierByEmailAndPassword(String email, String password);

    Courier findCourierByEmail(String email);
}
