package com.cps.delivery.repository;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.OrderItemStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends CrudRepository<OrderItem, Integer> {

    @Override
    Iterable<OrderItem> findAll();

    OrderItem findById(int id);

    // Displaying a list for each courier where orderItem status is only created
    List<OrderItem> findOrderItemByStatus(OrderItemStatus status);

    List<OrderItem> findByDispatcherId(int dispatcherId);

    List<OrderItem> findByCourierId(int courierId);

    List<OrderItem> findOrderItemsByStatus(OrderItemStatus status);
}
