package com.cps.delivery.controllers;

import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.User;
import com.cps.delivery.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {

    private final AdminService adminService;
    private final DispatcherService dispatcherService;
    private final CourierService courierService;
    private final MapValidationErrorService mapValidationErrorService;
    private final OrderService orderService;
    private final UserService userService;

    @Autowired
    public AdminController(AdminService adminService, DispatcherService dispatcherService,
                           CourierService courierService, OrderService orderService,
                           MapValidationErrorService mapValidationErrorService, UserService userService) {
        this.adminService = adminService;
        this.dispatcherService = dispatcherService;
        this.mapValidationErrorService = mapValidationErrorService;
        this.courierService = courierService;
        this.orderService = orderService;
        this.userService = userService;
    }

    // Methods to manage users:

    @GetMapping("/users/all")
    public Iterable<User> getAllUsers() {
        return userService.findAllUsers();
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getUserById(@PathVariable int userId) {

        User user = userService.findById(userId);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping("/user/update")
    public ResponseEntity<?> updateUser(@Valid @RequestBody User user, BindingResult result) {

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);

        if (errorMap != null) {
            return errorMap;
        }

        userService.updateUser(user);
        return new ResponseEntity<>("User was updated successfully", HttpStatus.OK);
    }

    @DeleteMapping("/user/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable int userId) {

        userService.deleteUserById(userId);
        return new ResponseEntity<>("User with ID " + userId + " was deleted", HttpStatus.OK);
    }

    // Methods to manage order items:

    @GetMapping("/order/all")
    public Iterable<OrderItem> getAllOrders() {
        return orderService.findAllOrders();
    }

    @GetMapping("/order/{orderId}")
    public ResponseEntity<OrderItem> getOrderById(@PathVariable int orderId) {

        OrderItem item = orderService.findById(orderId);

        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @PutMapping("/order/update")
    public ResponseEntity<?> updateOrder(@Valid @RequestBody OrderItem orderItem, BindingResult result) {

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);

        if (errorMap != null) {
            return errorMap;
        }

        orderService.updateOrder(orderItem);
        return new ResponseEntity<>("Order was updated successfully", HttpStatus.OK);
    }

    @DeleteMapping("/order/{orderId}")
    public ResponseEntity<String> deleteOrder(@PathVariable int orderId) {

        orderService.deleteOrder(orderId);
        return new ResponseEntity<>("Order with ID " + orderId + " was deleted", HttpStatus.OK);
    }
}
