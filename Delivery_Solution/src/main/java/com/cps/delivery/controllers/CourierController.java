package com.cps.delivery.controllers;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Rejection;
import com.cps.delivery.service.CourierService;
import com.cps.delivery.service.MapValidationErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/courier")
@CrossOrigin
public class CourierController {

    private final CourierService courierService;
    private final MapValidationErrorService mapValidationErrorService;

    @Autowired
    public CourierController(CourierService courierService, MapValidationErrorService mapValidationErrorService) {
        this.courierService = courierService;
        this.mapValidationErrorService = mapValidationErrorService;
    }

    @DeleteMapping("/{courierId}")
    public ResponseEntity<?> deleteCourier(@PathVariable("courierId") int courierId) {

        courierService.deleteCourierById(courierId);
        return new ResponseEntity<>("Courier with ID '" + courierId + "' was deleted successfully", HttpStatus.OK);
    }

    @GetMapping("{courierId}")
    public ResponseEntity<?> getCourierById(@PathVariable("courierId") int courierId) {

        Courier theCourier = courierService.findById(courierId);
        return new ResponseEntity<>(theCourier, HttpStatus.OK);
    }

    @GetMapping("/all")
    public Iterable<Courier> getAllCouriers() {

        return courierService.findAllCouriers();
    }

    @GetMapping("/{courierId}/rejected")
    public Iterable<Rejection> getCourierRejections(@PathVariable("courierId") int courierId) {
        return courierService.findCouriersRejections(courierId);
    }
}
