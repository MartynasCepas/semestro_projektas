package com.cps.delivery.controllers;

import com.cps.delivery.domain.User;
import com.cps.delivery.service.MapValidationErrorService;
import com.cps.delivery.service.UserService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    private final UserService userService;
    private final MapValidationErrorService mapValidationErrorService;

    @Autowired
    public UserController(UserService userService, MapValidationErrorService mapValidationErrorService) {
        this.userService = userService;
        this.mapValidationErrorService = mapValidationErrorService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody User user, BindingResult result) {

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);

        if (errorMap != null) {
            return errorMap;
        }

        userService.registerUser(user);

        return new ResponseEntity<>("User was created successfully", HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<?> getUserWhenLogin(@RequestBody User user) {

        return userService.findUserWhenLogin(user.getEmail(), user.getPassword());
    }

    @PutMapping("/change/{userId}")
    public ResponseEntity<?> changePassword(@PathVariable("userId") int userId, @RequestBody ObjectNode node){
        return userService.changePassword(node, userId);
    }

    @PutMapping("/resetPassword")
    public ResponseEntity<?> resetPassword(@RequestBody ObjectNode node) {
        return userService.resetPassword(node);
    }

    @GetMapping("/{email}")
    public ResponseEntity<?> getByEmail(@PathVariable("email") String email){

        User user = userService.findByEmail(email);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
