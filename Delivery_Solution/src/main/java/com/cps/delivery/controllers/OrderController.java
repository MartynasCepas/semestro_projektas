package com.cps.delivery.controllers;

import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.OrderItemStatus;
import com.cps.delivery.service.MapValidationErrorService;
import com.cps.delivery.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/order")
@CrossOrigin
public class OrderController {

    private final OrderService orderService;
    private final MapValidationErrorService mapValidationErrorService;

    @Autowired
    public OrderController(OrderService orderService, MapValidationErrorService mapValidationErrorService) {
        this.orderService = orderService;
        this.mapValidationErrorService = mapValidationErrorService;
    }

    @GetMapping("/user/{userId}")
    public List<OrderItem> getUserItemOrders(@PathVariable("userId") int userId) {
        return orderService.getUserOrders(userId);
    }

    // Filter
    @GetMapping("/user/filterStatus/{userId}/{status}")
    public List<OrderItem> getFilteredOrdersByStatus(@PathVariable("userId") int userId, @PathVariable("status") String status) {
        return orderService.filterOrdersByStatus(userId, status);
    }

    @GetMapping("/user/filterCategory/{userId}/{category}")
    public List<OrderItem> getFilteredOrdersByCategory(@PathVariable("userId") int userId, @PathVariable("category") String category) {
        return orderService.filterOrdersByCategory(userId, category);
    }

    // Sort
    // By order price ASCENDING
    @GetMapping("/user/sortByPriceAsc/{userId}")
    public List<OrderItem> getSortedOrdersByPriceAsc(@PathVariable("userId") int userId) {
        return orderService.sortOrdersByPriceAsc(userId);
    }

    // By order price DESCENDING
    @GetMapping("/user/sortByPriceDsc/{userId}")
    public List<OrderItem> getSortedOrdersByPriceDsc(@PathVariable("userId") int userId) {
        return orderService.sortOrdersByPriceDsc(userId);
    }

    // By order creation date ASCENDING (from oldest to newest)
    @GetMapping("/user/sortByCreatedDateAsc/{userId}")
    public List<OrderItem> getSortedOrdersByCreatedDateAsc(@PathVariable("userId") int userId) {
        return orderService.sortOrdersByCreatedDateAsc(userId);
    }

    // By order creation date DESCENDING (from newest to oldest)
    @GetMapping("/user/sortByCreatedDateDsc/{userId}")
    public List<OrderItem> getSortedOrdersByCreatedDateDsc(@PathVariable("userId") int userId) {
        return orderService.sortOrdersByCreatedDateDsc(userId);
    }

    @GetMapping("/all")
    public Iterable<OrderItem> getAllOrders() {
        return orderService.findAllOrders();
    }

    @GetMapping("/created")
    public Iterable<OrderItem> getAllOrdersCreated() {
        return orderService.findOrdersWithStatusCreated();
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<OrderItem> getOrderById(@PathVariable int orderId) {

        OrderItem item = orderService.findById(orderId);

        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @GetMapping("/available/{courierId}")
    public Iterable<OrderItem> getUniqueListForCourier(@PathVariable("courierId") int courierId) {
        return orderService.findUniqueOrders(courierId);
    }

    @PostMapping("/user/{dispatcherId}")
    public ResponseEntity<?> createOrderItem(@PathVariable("dispatcherId") int dispatcherId,
                                             @Valid @RequestBody OrderItem orderItem, BindingResult result) {

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);

        if (errorMap != null) {
            return errorMap;
        }

        OrderItem item = new OrderItem(orderItem.getWeight(), orderItem.getDimensions(), orderItem.getOrigin(),
                orderItem.getDestination(), orderItem.getComment(), orderItem.getLongitude(), orderItem.getLatitude(),
                orderItem.getCategory(), orderItem.getTitle(), orderItem.getPrice());

        orderService.createOrderItem(item, dispatcherId);
        return new ResponseEntity<>("Order was created successfully", HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateOrder(@Valid @RequestBody OrderItem orderItem, BindingResult result) {

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);

        if (errorMap != null) {
            return errorMap;
        }

        orderService.updateOrder(orderItem);
        return new ResponseEntity<>("Order was updated successfully", HttpStatus.OK);
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<String> deleteOrder(@PathVariable int orderId) {

        orderService.deleteOrder(orderId);
        return new ResponseEntity<>("Order with ID " + orderId + " was deleted", HttpStatus.OK);
    }

    @PostMapping("/{courierId}/{orderId}")
    public ResponseEntity<?> setOrderToCourier(@PathVariable("courierId") int courierId,
                                               @PathVariable("orderId") int orderId ) {

        orderService.setCourierAndSave(courierId, orderId);

        return new ResponseEntity<>("Order with ID '" + orderId + "' was set for User with ID '" + courierId + "'.", HttpStatus.OK);
    }

    @PostMapping("/status/{orderId}/{status}")
    @Async
    public ResponseEntity<?> changeStatus(@PathVariable("orderId") int orderId,
                                          @PathVariable("status") String status) {

        OrderItemStatus newStatus = orderService.convertStatusToObject(status);

        orderService.changeStatus(orderId, newStatus);

        return new ResponseEntity<>("Order status was changed successfully", HttpStatus.OK);
    }

    @GetMapping("/courier/{courierId}/all")
    public Iterable<OrderItem> getCourierOrders(@PathVariable("courierId") int courierId) {
        return orderService.getCourierOrders(courierId);
    }
}
