package com.cps.delivery.controllers;

import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.service.DispatcherService;
import com.cps.delivery.service.MapValidationErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/dispatcher")
@CrossOrigin
public class DispatcherController {

    private final MapValidationErrorService mapValidationErrorService;
    private final DispatcherService dispatcherService;

    @Autowired
    public DispatcherController(MapValidationErrorService mapValidationErrorService, DispatcherService dispatcherService) {
        this.mapValidationErrorService = mapValidationErrorService;
        this.dispatcherService = dispatcherService;
    }

    @GetMapping("/all")
    public Iterable<Dispatcher> getAllDispatchers() {
        return dispatcherService.findAllDispatchers();
    }

    @GetMapping("/{dispatcherId}")
    public ResponseEntity<?> getDispatcherById(@PathVariable int dispatcherId) {

        Dispatcher dispatcher = dispatcherService.findById(dispatcherId);

        return new ResponseEntity<>(dispatcher, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateDispatcher(@Valid @RequestBody Dispatcher dispatcher, BindingResult result) {

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);

        if (errorMap != null) {
            return errorMap;
        }

        dispatcherService.insertOrUpdate(dispatcher);
        return new ResponseEntity<>("Dispatcher was updated successfully", HttpStatus.OK);
    }

    @DeleteMapping("/{dispatcherId}")
    public ResponseEntity<?> deleteDispatcher(@PathVariable int dispatcherId) {

        dispatcherService.deleteDispatcher(dispatcherId);
        return new ResponseEntity<>("Dispatcher with ID " + dispatcherId + " was deleted", HttpStatus.OK);
    }
}
