package com.cps.delivery.controllers;

import com.cps.delivery.service.MapValidationErrorService;
import com.cps.delivery.service.OrderService;
import com.cps.delivery.service.RejectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rejected")
@CrossOrigin
public class RejectionController {

    private final RejectionService rejectionService;
    private final MapValidationErrorService mapValidationErrorService;

    @Autowired
    public RejectionController(RejectionService rejectionService, MapValidationErrorService mapValidationErrorService) {
        this.rejectionService = rejectionService;
        this.mapValidationErrorService = mapValidationErrorService;
    }

    @GetMapping("/{courierId}/all")
    public Iterable getRejectedOrders(@PathVariable("courierId") int courierId) {
        return rejectionService.getCourierRejections(courierId);
    }

    @GetMapping("/all")
    public Iterable getAllRejectedOrders() {
        return rejectionService.getAllRejections();
    }

    @PostMapping("/{courierId}/{orderId}")
    public ResponseEntity<?> rejectOrder(@PathVariable("courierId") int courierId, @PathVariable("orderId") int orderId) {

        rejectionService.rejectOrder(courierId, orderId);

        return new ResponseEntity<>("Order was successfully rejected", HttpStatus.OK);
    }
}
