package com.cps.delivery.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomRestExceptionHandler {

    // exception handler ... to catch any exception (catch all)

    @ExceptionHandler
    public ResponseEntity<CustomErrorResponse> handleException(Exception exc) {

        //  create a OrderErrorResponse

        CustomErrorResponse error = new CustomErrorResponse();

        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage(exc.getMessage());

        // return ResponseEntity

        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
