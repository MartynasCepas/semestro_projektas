package com.cps.delivery.exceptions;

public class CustomErrorException extends RuntimeException {

    public CustomErrorException(String message) {
        super(message);
    }

    public CustomErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomErrorException(Throwable cause) {
        super(cause);
    }
}
