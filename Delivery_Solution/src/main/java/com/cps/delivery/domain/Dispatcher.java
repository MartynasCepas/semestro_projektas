package com.cps.delivery.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Dispatcher extends User {

    @OneToMany(mappedBy = "dispatcher", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;

    private String type = "dispatcher";

    public Dispatcher() {

    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
