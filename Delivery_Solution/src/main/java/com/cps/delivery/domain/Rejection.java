package com.cps.delivery.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Rejection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JsonIgnore
    private OrderItem rejectedItems;

    @ManyToOne
    @JsonIgnore
    private Courier rejectionsForCourier;

    public Rejection() {

    }

    public Rejection(int id, OrderItem rejectedItem, Courier courier) {
        this.id = id;
        rejectedItems = rejectedItem;
        rejectionsForCourier = courier;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OrderItem getRejectedItems() {
        return rejectedItems;
    }

    public void setRejectedItems(OrderItem rejectedItems) {
        this.rejectedItems = rejectedItems;
    }

    public Courier getRejectionsForCourier() {
        return rejectionsForCourier;
    }

    public void setRejectionsForCourier(Courier rejectionsForCourier) {
        this.rejectionsForCourier = rejectionsForCourier;
    }
}
