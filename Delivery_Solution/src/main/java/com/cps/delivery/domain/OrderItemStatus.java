package com.cps.delivery.domain;

public enum OrderItemStatus {

    CREATED ("Created"),
    ACCEPTED ("Accepted"),
    REJECTED ("Rejected"),
    DELIVERED ("Delivered"),
    IN_TRANSIT ("In transit"),
    FAILED_TO_DELIVER ("Failed to deliver");

    private final String name;

    OrderItemStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
