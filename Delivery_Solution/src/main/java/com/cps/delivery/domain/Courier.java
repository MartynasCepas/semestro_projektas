package com.cps.delivery.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Courier extends User {

    @OneToMany(mappedBy = "courier")
    private List<OrderItem> orders;

    @OneToMany(mappedBy = "rejectionsForCourier")
    private List<Rejection> couriersRejections;

    private String type = "courier";

    public List<OrderItem> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderItem> orders) {
        this.orders = orders;
    }

    public List<Rejection> getCouriersRejections() {
        return couriersRejections;
    }

    public void setCouriersRejections(List<Rejection> couriersRejections) {
        this.couriersRejections = couriersRejections;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @PreRemove
    public void setOrdersToNull() {
        orders.forEach(pub -> pub.setCourier(null));
        couriersRejections.forEach(pub -> pub.setRejectionsForCourier(null));
    }
}
