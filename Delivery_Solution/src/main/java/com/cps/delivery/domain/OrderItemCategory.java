package com.cps.delivery.domain;

public enum OrderItemCategory {

    RADIOACTIVE ("Radioactive"),
    FRAGILE ("Fragile"),
    NA ("Other"),
    DRUGS ("Drugs"),
    HARD ("Hard");

    private final String name;

    OrderItemCategory(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
