package com.cps.delivery.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Range;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

@Entity
public class OrderItem extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotBlank(message = "Order weight is required")
    private String weight;
    @NotBlank(message = "Order dimensions are required")
    private String dimensions;
    @NotBlank(message = "Order origin is required")
    private String origin;
    @NotBlank(message = "Order destination is required")
    private String destination;
    @NotNull(message = "Order comment cannot be null")
    private String comment;

    private OrderItemStatus status;
    @Nullable
    private double longitude;
    @Nullable
    private double latitude;
    @NotBlank(message = "Order category is required")
    private String category;
    @NotBlank(message = "Order title is required")
    private String title;
    @DecimalMin(value = "0.01", message = "Price must be greater than 0")
    @Digits(integer=3, fraction=2)
    @NotNull
    private BigDecimal price;

    // Dispatcher
    @ManyToOne
    @JsonIgnore
    private Dispatcher dispatcher;

    // Courier
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE,
                            CascadeType.DETACH, CascadeType.REFRESH})
    @JsonIgnore
    private Courier courier;

    @OneToMany(mappedBy = "rejectedItems", cascade = CascadeType.ALL)
    private List<Rejection> ordersForRejection;

    public OrderItem() {

    }

    public OrderItem(int id) {
        this.id = id;
    }

    public OrderItem(String weight, String dimensions, String origin, String destination, String comment,
                     double lng, double lat, String category, String title, BigDecimal price) {
        this.weight = weight;
        this.dimensions = dimensions;
        this.origin = origin;
        this.destination = destination;
        this.comment = comment;
        this.latitude = lat;
        this.longitude = lng;
        this.category = category;
        this.title = title;
        this.price = price;
    }

    public static Comparator<OrderItem> ItemPriceComparator = new Comparator<OrderItem>() {

        public int compare(OrderItem o1, OrderItem o2) {
            BigDecimal p1 = o1.getPrice();
            BigDecimal p2 = o2.getPrice();
            int int1 = p1.intValueExact();
            int int2 = p2.intValueExact();

            /* For ascending order */
            return int1-int2;

            /* For descending order */
            // return int2-int1;
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public Courier getCourier() {
        return courier;
    }

    public void setCourier(Courier courier) {
        this.courier = courier;
    }

    public OrderItemStatus getStatus() {
        return status;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setStatus(OrderItemStatus status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<Rejection> getOrdersForRejection() {
        return ordersForRejection;
    }

    public void setOrdersForRejection(List<Rejection> ordersForRejection) {
        this.ordersForRejection = ordersForRejection;
    }
}
