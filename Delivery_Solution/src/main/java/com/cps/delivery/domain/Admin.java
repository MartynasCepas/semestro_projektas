package com.cps.delivery.domain;

import javax.persistence.Entity;

@Entity
public class Admin extends User {

    private String type = "admin";

    public Admin() {

    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }
}
