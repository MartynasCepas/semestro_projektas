package com.cps.delivery.service;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.domain.Rejection;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.CourierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourierService {

    private final CourierRepository courierRepository;

    @Autowired
    public CourierService(CourierRepository courierRepository) {
        this.courierRepository = courierRepository;
    }

    public void insertOrUpdate(Courier theCourier) {
        courierRepository.save(theCourier);
    }

    public void deleteCourierById(int courierId) {

        if (courierRepository.findById(courierId) == null) {
            throw new CustomErrorException("Courier with ID '" + courierId + "' not found");
        }
        courierRepository.deleteById(courierId);
    }

    public Courier findById(int courierId) {

        Courier theCourier = courierRepository.findById(courierId);

        if (theCourier == null) {
            throw new CustomErrorException("Courier with ID '" + courierId + "' not found");
        }

        return theCourier;
    }

    public Iterable<Courier> findAllCouriers() {
        return courierRepository.findAll();
    }

    public Iterable<Rejection> findCouriersRejections(int courierId) {
        return courierRepository.findCourierById(courierId);
    }
}
