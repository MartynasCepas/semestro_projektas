package com.cps.delivery.service;

import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.DispatcherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DispatcherService {

    private final DispatcherRepository dispatcherRepository;

    @Autowired
    public DispatcherService(DispatcherRepository dispatcherRepository) {
        this.dispatcherRepository = dispatcherRepository;
    }

    public void insertOrUpdate(Dispatcher dispatcher) {
        dispatcherRepository.save(dispatcher);
    }

    public Iterable<Dispatcher> findAllDispatchers() {
        return dispatcherRepository.findAll();
    }

    public void deleteDispatcher(int id) {

        if (dispatcherRepository.findById(id) == null) {
            throw new CustomErrorException("Dispatcher doesn't exist, check dispatcher id");
        }
        dispatcherRepository.deleteById(id);
    }

    public Dispatcher findById(int id) {

        Dispatcher dispatcher = dispatcherRepository.findById(id);

        if (dispatcher == null) {
            throw new CustomErrorException(("Dispatcher by ID '" + id + "' was not found"));
        }
        return dispatcher;
    }

    public Dispatcher findDispatcherByEmailAndPassword(String email, String password) {
        Dispatcher dispatcher = dispatcherRepository.findDispatcherByEmailAndPassword(email, password);
        return dispatcher;
    }

    public Dispatcher findDispatcherByEmail(String email) {
        Dispatcher dispatcher = dispatcherRepository.findDispatcherByEmail(email);
        return dispatcher;
    }
}
