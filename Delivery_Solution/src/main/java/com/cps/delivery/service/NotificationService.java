package com.cps.delivery.service;

import com.cps.delivery.domain.OrderItemStatus;
import com.cps.delivery.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Async
public class NotificationService {

    private JavaMailSender javaMailSender;

    @Autowired
    public NotificationService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void sendEmailWhenOrderStatusChange(User theUser, OrderItemStatus newStatus, int id) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(theUser.getEmail());
        mail.setSubject("Order update");
        mail.setText("Hi, your order (ID - " + id + " ) status is now set to " + newStatus + ".");

        javaMailSender.send(mail);
    }

    @Async
    public void sendNewPassword(User user,String newPassword) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(user.getEmail());
        mail.setSubject("Password reset");
        mail.setText("There was a request to change your password. Your new password is: " + newPassword);

        javaMailSender.send(mail);
    }
}
