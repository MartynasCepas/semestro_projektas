package com.cps.delivery.service;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.Rejection;
import com.cps.delivery.repository.CourierRepository;
import com.cps.delivery.repository.OrderRepository;
import com.cps.delivery.repository.RejectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RejectionService {

    private final OrderRepository orderRepository;
    private final CourierRepository courierRepository;
    private final RejectionRepository rejectionRepository;

    @Autowired
    public RejectionService(OrderRepository orderRepository, CourierRepository courierRepository, RejectionRepository rejectionRepository) {
        this.orderRepository = orderRepository;
        this.courierRepository = courierRepository;
        this.rejectionRepository = rejectionRepository;
    }

    public Iterable<Rejection> getCourierRejections(int courierId) {
        return rejectionRepository.findRejectionsByRejectionsForCourierId(courierId);
    }

    public Iterable<Rejection> getAllRejections() {
        return rejectionRepository.findAll();
    }

    public void rejectOrder(int courierId, int orderId) {

        Rejection theRejection = new Rejection();
        Courier theCourier = courierRepository.findById(courierId);
        OrderItem theItem = orderRepository.findById(orderId);

        theRejection.setRejectionsForCourier(theCourier);
        theRejection.setRejectedItems(theItem);
        rejectionRepository.save(theRejection);
    }
}
