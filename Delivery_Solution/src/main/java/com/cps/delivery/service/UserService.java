package com.cps.delivery.service;

import com.cps.delivery.domain.Admin;
import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.domain.User;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.RandomStringUtils;

@Service
public class UserService {

    private final DispatcherRepository dispatcherRepository;
    private final CourierRepository courierRepository;
    private final UserRepository userRepository;
    private final AdminRepository adminRepository;
    private final NotificationService notificationService;

    @Autowired
    public UserService(DispatcherRepository dispatcherRepository, CourierRepository courierRepository,
                       UserRepository userRepository, AdminRepository adminRepository, NotificationService notificationService) {
        this.dispatcherRepository = dispatcherRepository;
        this.courierRepository = courierRepository;
        this.userRepository = userRepository;
        this.adminRepository = adminRepository;
        this.notificationService = notificationService;
    }

    public void registerUser(User user) {

        if (userRepository.findByEmail(user.getEmail()) == null) {
            if (user.getType().equals("courier")) {
                Courier courier = new Courier();
                courier.setFirstName(user.getFirstName());
                courier.setLastName(user.getLastName());
                courier.setEmail(user.getEmail());
                courier.setPassword(user.getPassword());
                courier.setPhoneNumber(user.getPhoneNumber());
                courierRepository.save(courier);
            }
            else if (user.getType().equals("dispatcher")) {
                Dispatcher dispatcher = new Dispatcher();
                dispatcher.setFirstName(user.getFirstName());
                dispatcher.setLastName(user.getLastName());
                dispatcher.setEmail(user.getEmail());
                dispatcher.setPassword(user.getPassword());
                dispatcher.setPhoneNumber(user.getPhoneNumber());
                dispatcherRepository.save(dispatcher);
            }
            else if(user.getType().equals("admin")) {
                Admin admin = new Admin();
                admin.setFirstName(user.getFirstName());
                admin.setLastName(user.getLastName());
                admin.setEmail(user.getEmail());
                admin.setPassword(user.getPassword());
                admin.setPhoneNumber(user.getPhoneNumber());
                adminRepository.save(admin);
            }
        }
        else {
            throw new CustomErrorException("Email is taken!");
        }
    }

    public ResponseEntity<?> findUserWhenLogin(String email, String password) {

        User theUser = userRepository.findByEmail(email);

        Map<String,String> errorMap = new HashMap<>();

        if(theUser == null){
            errorMap.put("email","Email is incorrect");
            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }
        if(userRepository.findByEmailAndPassword(email, password) == null){
            errorMap.put("password","Password is incorrect");
            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(theUser, HttpStatus.OK);
    }

    public User findByEmail(String email){
        User user = userRepository.findByEmail(email);

        if (user == null) {
            throw new CustomErrorException("User with email '" + email + "' not found");
        }

        return user;
    }

    public ResponseEntity<?> changePassword(ObjectNode node, int userId){

        String oldPassword = node.get("oldPassword").asText();
        String newPassword = node.get("newPassword").asText();

        User user = userRepository.findById(userId);

        Map<String, String> errors = new HashMap<>();
        errors.put("oldPassword", null);
        errors.put("newPassword", null);

        if (user == null) {
            throw new CustomErrorException("User was not found");
        }

        if (!oldPassword.equals(user.getPassword())) {
            errors.replace("oldPassword", "Old password was incorrect");
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }

        if (newPassword.isEmpty()) {
            errors.replace("newPassword", "Please fix your password");
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }

        if (oldPassword.equals(newPassword)) {
            errors.replace("newPassword", "Old password is the same as new password");
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }

        user.setPassword(newPassword);
        userRepository.save(user);
        return new ResponseEntity<>("Password was changed successfully", HttpStatus.OK);
    }

    @Async
    public ResponseEntity<?> resetPassword(ObjectNode node) {
        String email = node.get("email").asText();

        User user = userRepository.findByEmail(email);
        Map<String, String> errors = new HashMap<>();
        errors.put("email", null);

        if (user == null) {
            errors.replace("email", "There is no user with given email address");
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }

        String newPassword = generatePassword();

        user.setPassword(newPassword);
        userRepository.save(user);
        notificationService.sendNewPassword(user, newPassword);

        return new ResponseEntity<>("We have sent a new password to specified email address", HttpStatus.OK);
    }

    public String generatePassword() {
        char[] possibleCharacters = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")).toCharArray();
        String password = RandomStringUtils.random(8, 0, possibleCharacters.length-1, false, false, possibleCharacters, new SecureRandom());
        return password;
    }

    public Iterable<User> findAllUsers() {
        return userRepository.findAll();
    }

    public void insertOrUpdate(User user) {
        userRepository.save(user);
    }

    public void updateUser(User user) {
        int userId = user.getId();
        User theUser = userRepository.findById(userId);

        theUser.setFirstName(user.getFirstName());
        theUser.setLastName((user.getLastName()));
        theUser.setEmail(user.getEmail());
        theUser.setPassword(user.getPassword());
        theUser.setPhoneNumber(user.getPhoneNumber());
        theUser.setType(user.getType());

        userRepository.save(theUser);
    }

    public void deleteUserById(int userId) {

        if (userRepository.findById(userId) == null) {
            throw new CustomErrorException("User with ID '" +  userId + "' not found");
        }
        userRepository.deleteById(userId);
    }

    public User findById(int userId) {

        User user = userRepository.findById(userId);

        if (user == null) {
            throw new CustomErrorException("User with ID '" + userId + "' not found");
        }

        return user;
    }
}
