package com.cps.delivery.service;

import com.cps.delivery.domain.*;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cps.delivery.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final DispatcherRepository dispatcherRepository;
    private final CourierRepository courierRepository;
    private final RejectionRepository rejectionRepository;
    private final UserRepository userRepository;
    private final AdminRepository adminRepository;
    private final NotificationService notificationService;


    @Autowired
    public OrderService(OrderRepository orderRepository, DispatcherRepository dispatcherRepository,
                        CourierRepository courierRepository, RejectionRepository rejectionRepository,
                        UserRepository userRepository, AdminRepository adminRepository, NotificationService notificationService) {
        this.orderRepository = orderRepository;
        this.dispatcherRepository = dispatcherRepository;
        this.courierRepository = courierRepository;
        this.rejectionRepository = rejectionRepository;
        this.userRepository = userRepository;
        this.adminRepository = adminRepository;
        this.notificationService = notificationService;
    }

    public void createOrderItem(OrderItem orderItem, int dispatcherId) {
        Dispatcher dispatcher = dispatcherRepository.findById(dispatcherId);

        if (dispatcher == null) {
            throw new CustomErrorException("Dispatcher was not found");
        }

        orderItem.setDispatcher(dispatcher);
        orderItem.setStatus(OrderItemStatus.CREATED);
        orderRepository.save(orderItem);
    }

    public void updateOrder(OrderItem orderItem) {
        int oldItemId = orderItem.getId();
        OrderItem item = orderRepository.findById(oldItemId);

        item.setOrigin(orderItem.getOrigin());
        item.setWeight(orderItem.getWeight());
        item.setDestination(orderItem.getDestination());
        item.setDimensions(orderItem.getDimensions());
        item.setComment(orderItem.getComment());
        item.setLatitude(orderItem.getLatitude());
        item.setLongitude(orderItem.getLongitude());
        item.setCategory(orderItem.getCategory());
        item.setTitle(orderItem.getTitle());
        item.setPrice(orderItem.getPrice());

        orderRepository.save(item);
    }

    public void setCourierAndSave(int courierId, int orderId) {
        OrderItem orderItem = orderRepository.findById(orderId);
        Courier courier = courierRepository.findById(courierId);

        if (orderItem == null) {
            throw new CustomErrorException("Order is already set for another courier.");
        }

        if (courier == null) {
            throw new CustomErrorException("Courier was not found");
        }

        orderItem.setCourier(courier);
        orderItem.setStatus(OrderItemStatus.ACCEPTED);
        orderRepository.save(orderItem);
    }

    public OrderItemStatus convertStatusToObject(String status) {
        for (OrderItemStatus type : OrderItemStatus.values()) {
            if (type.getName().equals(status)) {
                return type;
            }
        }

        return null;
    }

    public OrderItemCategory convertCategoryToObject(String category) {
        for (OrderItemCategory type : OrderItemCategory.values()) {
            if (type.getName().equals(category)) {
                return type;
            }
        }
        return null;
    }
    @Async
    public void changeStatus(int orderId, OrderItemStatus newStatus) {
        OrderItem orderItem = orderRepository.findById(orderId);
        OrderItemStatus currentStatus = orderItem.getStatus();

        switch (currentStatus) {
            case CREATED:
                switch (newStatus) {
                    case ACCEPTED:
                        User theUser = userRepository.findById(orderItem.getDispatcher().getId());
                        notificationService.sendEmailWhenOrderStatusChange(theUser, newStatus, orderId);
                        break;
                    case REJECTED:
                        orderItem.setStatus(newStatus);
                        orderRepository.save(orderItem);
                        break;
                    default:
                        throw new CustomErrorException("That's a big bug");
                }
                break;

            case ACCEPTED:
                switch (newStatus) {
                    case IN_TRANSIT:
                        orderItem.setStatus(newStatus);
                        orderRepository.save(orderItem);

                        User theUser = userRepository.findById(orderItem.getDispatcher().getId());
                        notificationService.sendEmailWhenOrderStatusChange(theUser, newStatus, orderId);
                        break;
                    default:
                        throw new CustomErrorException("This is weird, can't do that");
                }
                break;

            case IN_TRANSIT:
                switch (newStatus) {
                    case DELIVERED:
                    case FAILED_TO_DELIVER:
                        orderItem.setStatus(newStatus);
                        orderRepository.save(orderItem);

                        User theUser = userRepository.findById(orderItem.getDispatcher().getId());
                        notificationService.sendEmailWhenOrderStatusChange(theUser, newStatus, orderId);
                        break;
                    default:
                        throw new CustomErrorException("Another one. I mean error");
                }
                break;

            default:
                throw new CustomErrorException("RIP");

        }
    }

    public Iterable<OrderItem> findAllOrders() {
        return orderRepository.findAll();
    }

    public Iterable<OrderItem> findOrdersWithStatusCreated() {
        return orderRepository.findOrderItemsByStatus(OrderItemStatus.CREATED);
    }

    public Iterable<OrderItem> findUniqueOrders(int courierId) {
        List<OrderItem> orderItems = new ArrayList<>();
        List<Rejection> rejectionsForCourier = new ArrayList<>();
        //List<OrderItem> uniqueItems = new ArrayList<>();

        orderRepository.findAll().forEach(orderItems::add);

        rejectionRepository.findRejectionsByRejectionsForCourierId(courierId).forEach(rejectionsForCourier::add);

        for (int j = 0; j < rejectionsForCourier.size(); j++) {

            var tmpi = rejectionsForCourier.get(j).getRejectedItems().getId();
            OrderItem tmpItem = orderRepository.findById(tmpi);
            var tmpc = rejectionsForCourier.get(j).getRejectionsForCourier().getId();
            for (int i = 0; i < orderItems.size(); i++) {
                if (tmpi == orderItems.get(i).getId() && tmpc == courierId) {
                    orderItems.remove(orderItems.get(i));
                }
            }
        }

        return orderItems;
    }

    public void deleteOrder(int id) {
        if (orderRepository.findById(id) == null) {
            throw new CustomErrorException("Order doesn't exist, check order id");
        }

        orderRepository.deleteById(id);
    }

    public OrderItem findById(int id) {
        OrderItem item = orderRepository.findById(id);

        if (orderRepository.findById(id) == null) {
            throw new CustomErrorException("Order with ID '" + id + "' not found");
        }

        return item;
    }

    public List<OrderItem> getUserOrders(int userId) {
        List<OrderItem> orderItems = new ArrayList<>();
        User user = userRepository.findById(userId);
        if (user == null) {
            throw new CustomErrorException("User was not found");
        }

        if (user.getType() == "dispatcher") {
            orderRepository.findByDispatcherId(userId).forEach(orderItems::add);
        }
        return orderItems;
    }

    public List<OrderItem> getCourierOrders(int courierId) {
        List<OrderItem> orderItems = new ArrayList<>();
        orderRepository.findByCourierId(courierId).forEach(orderItems::add);
        return orderItems;
    }

    public List<OrderItem> filterOrdersByStatus(int userId, String status) {
        List<OrderItem> orderItems = new ArrayList<>();
        List<OrderItem> filteredItems = new ArrayList<>();

        OrderItemStatus filterStatus = convertStatusToObject(status);
        User user = userRepository.findById(userId);

        if (user != null) {
            if (user.getType().equals("dispatcher")) {
                orderRepository.findByDispatcherId(userId).forEach(orderItems::add);
                for (int i = 0; i < orderItems.size(); i++) {
                    if (orderItems.get(i).getStatus().equals(filterStatus)) {
                        filteredItems.add(orderItems.get(i));
                    }
                }
            }
            if (user.getType().equals("courier")) {
                orderRepository.findByCourierId(userId).forEach(orderItems::add);
                for (int i = 0; i < orderItems.size(); i++) {
                    if (orderItems.get(i).getStatus().equals(filterStatus)) {
                        filteredItems.add(orderItems.get(i));
                    }
                }
            }
            if (user.getType().equals("admin")) {
                orderRepository.findAll().forEach(orderItems::add);
                for (int i = 0; i < orderItems.size(); i++) {
                    if (orderItems.get(i).getStatus().equals(filterStatus)) {
                        filteredItems.add(orderItems.get(i));
                    }
                }
            }
        }
        return filteredItems;
    }

    public List<OrderItem> filterOrdersByCategory(int userId, String category) {
        List<OrderItem> orderItems = new ArrayList<>();
        List<OrderItem> filteredItems = new ArrayList<>();

        OrderItemCategory filterCategory = convertCategoryToObject(category);
        User user = userRepository.findById(userId);

        if (user != null) {
            if (user.getType().equals("dispatcher")) {
                orderRepository.findByDispatcherId(userId).forEach(orderItems::add);
                for (int i = 0; i < orderItems.size(); i++) {
                    if (orderItems.get(i).getCategory().equals(category)) {
                        filteredItems.add(orderItems.get(i));
                    }
                }
            }
            if (user.getType().equals("courier")) {
                orderRepository.findByCourierId(userId).forEach(orderItems::add);
                for (int i = 0; i < orderItems.size(); i++) {
                    if (orderItems.get(i).getCategory().equals(category)) {
                        filteredItems.add(orderItems.get(i));
                    }
                }
            }
            if (user.getType().equals("admin")) {
                orderRepository.findAll().forEach(orderItems::add);
                for (int i = 0; i < orderItems.size(); i++) {
                    if (orderItems.get(i).getCategory().equals(category)) {
                        filteredItems.add(orderItems.get(i));
                    }
                }
            }
        }
        return filteredItems;
    }

    public List<OrderItem> sortOrdersByPriceAsc(int userId) {
        List<OrderItem> orderItems = new ArrayList<>();
        User user = userRepository.findById(userId);

        if (user != null) {
            if (user.getType().equals("dispatcher")) {
                orderRepository.findByDispatcherId(userId).forEach(orderItems::add);
                Collections.sort(orderItems, OrderItem.ItemPriceComparator);
            }
            if (user.getType().equals("courier")) {
                orderRepository.findByCourierId(userId).forEach(orderItems::add);
                Collections.sort(orderItems, OrderItem.ItemPriceComparator);
            }
            if (user.getType().equals("admin")) {
                orderRepository.findAll();
                Collections.sort(orderItems, OrderItem.ItemPriceComparator);
            }
        }
        return orderItems;
    }

    public List<OrderItem> sortOrdersByPriceDsc(int userId) {
        List<OrderItem> orderItems = sortOrdersByPriceAsc(userId);

        // Reverse list from ascending to descending
        Collections.reverse(orderItems);
        return orderItems;
    }

    public List<OrderItem> sortOrdersByCreatedDateAsc(int userId) {
        List<OrderItem> orderItems = new ArrayList<>();
        User user = userRepository.findById(userId);

        if (user != null) {
            if (user.getType().equals("dispatcher")) {
                orderRepository.findByDispatcherId(userId).forEach(orderItems::add);
                orderItems.sort(Comparator.comparing(OrderItem::getCreatedAt));
            }
            if (user.getType().equals("courier")) {
                orderRepository.findByCourierId(userId).forEach(orderItems::add);
                orderItems.sort(Comparator.comparing(OrderItem::getCreatedAt));
            }
            if (user.getType().equals("admin")) {
                orderRepository.findAll().forEach(orderItems::add);
                orderItems.sort(Comparator.comparing(OrderItem::getCreatedAt));
            }
        }
        return orderItems;
    }

    public List<OrderItem> sortOrdersByCreatedDateDsc(int userId) {
        List<OrderItem> orderItems = sortOrdersByCreatedDateAsc(userId);

        // Reverse list from ascending to descending
        Collections.reverse(orderItems);
        return orderItems;
    }
}
