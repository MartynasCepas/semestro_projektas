package com.cps.delivery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class DeliverySolutionApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeliverySolutionApplication.class, args);
    }
}

