package com.cps.delivery.controllers;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.Rejection;
import com.cps.delivery.domain.User;
import com.cps.delivery.service.CourierService;
import com.cps.delivery.service.MapValidationErrorService;
import com.cps.delivery.service.OrderService;
import com.cps.delivery.service.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CourierControllerTest {

    private CourierService courierService;
    private MapValidationErrorService mapValidationErrorService;
    private CourierController courierController;

    private int id;

    private Courier courier;

    @BeforeEach
    void setUp() {
        courierService = mock(CourierService.class);
        mapValidationErrorService = mock(MapValidationErrorService.class);
        id = 5;
        courier = new Courier();
        courierController = new CourierController(courierService, mapValidationErrorService);
    }

    @Test
    void deleteCourierWhenCourierNotExist() {
        doThrow(new IllegalArgumentException()).when(courierService).deleteCourierById(id);
        assertEquals(HttpStatus.BAD_REQUEST, courierController.deleteCourier(id).getStatusCode());
    }

    @Test
    void deleteCourierWhenCourierExists() {
        courierController.deleteCourier(id);
        verify(courierService).deleteCourierById(id);
        assertEquals(HttpStatus.OK, courierController.deleteCourier(id).getStatusCode());
    }

    @Test
    void getCourierByIdWhenIdNotExist() {
        when(courierService.findById(id)).thenReturn(null);
        assertEquals(HttpStatus.BAD_REQUEST, courierController.getCourierById(id).getStatusCode());
    }

    @Test
    void getCourierByIdWhenIdExists() {
        courier.setId(id);
        when(courierService.findById(id)).thenReturn(courier);
        assertEquals(HttpStatus.OK, courierController.getCourierById(id).getStatusCode());
    }

    @Test
    void getAllCouriersWhenCouriersNotExist() {
        when(courierService.findAllCouriers()).thenReturn(null);
        Assert.assertNull("No couriers found", courierController.getAllCouriers());
    }

    @Test
    void getAllCouriersWhenCouriersExists() {
        List<Courier> couriersList = new ArrayList<>();
        couriersList.add(new Courier());
        couriersList.add(new Courier());
        couriersList.add(new Courier());
        when(courierService.findAllCouriers()).thenReturn(couriersList);
        Iterable<Courier> couriers = courierController.getAllCouriers();
        assertEquals(couriersList, couriers);
    }

    @Test
    void getCourierRejections() {
        List<Rejection> rejectionList = new ArrayList<>();
        rejectionList.add(new Rejection());
        rejectionList.add(new Rejection());
        rejectionList.add(new Rejection());
        when(courierService.findCouriersRejections(id)).thenReturn(rejectionList);
        Iterable<Rejection> rejections = courierController.getCourierRejections(id);
        assertEquals(rejectionList, rejections);
    }
}