package com.cps.delivery.controllers;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.User;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.AdminRepository;
import com.cps.delivery.repository.CourierRepository;
import com.cps.delivery.repository.DispatcherRepository;
import com.cps.delivery.repository.UserRepository;
import com.cps.delivery.service.*;
import org.aspectj.weaver.ast.Or;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.rmi.MarshalledObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AdminControllerTest {

    private AdminService adminService;
    private DispatcherService dispatcherService;
    private CourierService courierService;
    private MapValidationErrorService mapValidationErrorService;
    private OrderService orderService;
    private UserService userService;
    private AdminController adminController;

    private int id;

    private User user;

    private OrderItem orderItem;

    private BindingResult result;

    @BeforeEach
    void setUp() {
        mapValidationErrorService = mock(MapValidationErrorService.class);
        orderService = mock(OrderService.class);
        userService = mock(UserService.class);
        adminController = new AdminController(adminService, dispatcherService, courierService, orderService, mapValidationErrorService, userService);
        user = new User();
        id = 5;
        result = mock(BindingResult.class);
        orderItem = new OrderItem();
    }

    @Test
    void getAllUsersWhenUsersNotExist() {
        when(userService.findAllUsers()).thenReturn(null);
        Assert.assertNull(null, adminController.getAllUsers());
    }

    @Test
    void getAllUsersWhenUsersExist() {
        List<User> usersList = new ArrayList<>();
        usersList.add(new User());
        usersList.add(new User());
        usersList.add(new User());
        when(userService.findAllUsers()).thenReturn(usersList);
        Iterable<User> users = adminController.getAllUsers();
        assertEquals(usersList, users);
    }

    @Test
    void getUserByIdWhenIdNotExist() {
        when(userService.findById(id)).thenReturn(null);
        assertEquals(HttpStatus.BAD_REQUEST, adminController.getUserById(id).getStatusCode());
    }

    @Test
    void getUserByIdWhenIdExists() {
        user.setId(id);
        when(userService.findById(id)).thenReturn(user);
        assertEquals(HttpStatus.OK, adminController.getUserById(id).getStatusCode());
    }

    @Test
    void updateUserWhenNoErrors() {
        user.setId(id);
        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(any(BindingResult.class));
        when(mapValidationErrorService.MapValidationService(any(BindingResult.class))).thenReturn(null);
        adminController.updateUser(user, any(BindingResult.class));
        verify(userService).updateUser(user);
        Assert.assertEquals("We have sent a new password to specified email address", "We have sent a new password to specified email address");
    }

    @Test
    void deleteUserWhenUserNotExist() {
        doThrow(new IllegalArgumentException()).when(userService).deleteUserById(id);
        assertEquals(HttpStatus.BAD_REQUEST, adminController.deleteUser(id).getStatusCode());
    }

    @Test
    void deleteUserWhenUserExists() {
        user.setId(id);
        adminController.deleteUser(id);
        verify(userService).deleteUserById(id);
        assertEquals(HttpStatus.OK, adminController.deleteUser(id).getStatusCode());
    }

    @Test
    void getAllOrders() {
        List<OrderItem> ordersList = new ArrayList<>();
        ordersList.add(new OrderItem());
        ordersList.add(new OrderItem());
        ordersList.add(new OrderItem());
        when(orderService.findAllOrders()).thenReturn(ordersList);
        Iterable<OrderItem> users = adminController.getAllOrders();
        assertEquals(ordersList, users);
    }

    @Test
    void getOrderByIdWhenIdNotExist() {
        orderItem.setId(id);
        when(orderService.findById(id)).thenReturn(null);
        assertEquals(HttpStatus.BAD_REQUEST, adminController.getOrderById(id).getStatusCode());
    }

    @Test
    void getOrderByIdWhenIdExists() {
        orderItem.setId(id);
        when(orderService.findById(id)).thenReturn(orderItem);
        assertEquals(HttpStatus.OK, adminController.getOrderById(id).getStatusCode());
    }

    @Test
    void updateOrderWhenNoErrors() {
        orderItem.setId(id);
        when(mapValidationErrorService.MapValidationService(any(BindingResult.class))).thenReturn(null);
        adminController.updateOrder(orderItem, any(BindingResult.class));
        verify(orderService).updateOrder(orderItem);
        Assert.assertEquals(HttpStatus.OK, adminController.updateOrder(orderItem, any(BindingResult.class)).getStatusCode());
    }

    @Test
    void deleteOrderWhenOrderNotExist() {
        doThrow(new IllegalArgumentException()).when(orderService).deleteOrder(id);
        assertEquals(HttpStatus.BAD_REQUEST, adminController.deleteOrder(id).getStatusCode());
    }

    @Test
    void deleteOrderWhenOrderExists() {
        orderItem.setId(id);
        adminController.deleteOrder(id);
        verify(orderService).deleteOrder(id);
        assertEquals(HttpStatus.OK, adminController.deleteOrder(id).getStatusCode());
    }
}