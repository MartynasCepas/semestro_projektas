package com.cps.delivery.controllers;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.service.CourierService;
import com.cps.delivery.service.DispatcherService;
import com.cps.delivery.service.MapValidationErrorService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DispatcherControllerTest {

    private DispatcherService dispatcherService;
    private MapValidationErrorService mapValidationErrorService;
    private DispatcherController dispatcherController;

    private int id;

    private Dispatcher dispatcher;

    @BeforeEach
    void setUp() {
        dispatcherService = mock(DispatcherService.class);
        mapValidationErrorService = mock(MapValidationErrorService.class);
        id = 5;
        dispatcher = new Dispatcher();
        dispatcherController = new DispatcherController(mapValidationErrorService, dispatcherService);
    }

    @Test
    void getAllDispatchersWhenDispatchersNotExist() {
        when(dispatcherService.findAllDispatchers()).thenReturn(null);
        Assert.assertNull("No dispatchers found", dispatcherController.getAllDispatchers());
    }

    @Test
    void getAllDispatchersWhenDispatchersExist() {
        List<Dispatcher> dispatcherList = new ArrayList<>();
        dispatcherList.add(new Dispatcher());
        dispatcherList.add(new Dispatcher());
        dispatcherList.add(new Dispatcher());
        when(dispatcherService.findAllDispatchers()).thenReturn(dispatcherList);
        Iterable<Dispatcher> dispatchers = dispatcherService.findAllDispatchers();
        assertEquals(dispatcherList, dispatchers);
    }

    @Test
    void getDispatcherByIdWhenIdNotExist() {
        when(dispatcherService.findById(id)).thenReturn(null);
        assertEquals(HttpStatus.BAD_REQUEST, dispatcherController.getDispatcherById(id).getStatusCode());
    }

    @Test
    void getDispatcherByIdWhenIdExists() {
        dispatcher.setId(id);
        when(dispatcherService.findById(id)).thenReturn(dispatcher);
        assertEquals(HttpStatus.OK, dispatcherController.getDispatcherById(id).getStatusCode());
    }

    @Test
    void updateDispatcher() {
    }

    @Test
    void deleteDispatcherWhenDispatcherNotExist() {
        doThrow(new IllegalArgumentException()).when(dispatcherService).deleteDispatcher(id);
        assertEquals(HttpStatus.BAD_REQUEST, dispatcherController.deleteDispatcher(id).getStatusCode());
    }

    @Test
    void deleteDispatcherWhenDispatcherExists() {
        dispatcherController.deleteDispatcher(id);
        verify(dispatcherService).deleteDispatcher(id);
        assertEquals(HttpStatus.OK, dispatcherController.deleteDispatcher(id).getStatusCode());
    }
}