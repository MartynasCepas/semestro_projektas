package com.cps.delivery.service;

import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.OrderItemStatus;
import com.cps.delivery.domain.User;
import com.cps.delivery.repository.*;
import javassist.Loader;
import org.aspectj.weaver.ast.Not;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class NotificationServiceTest {

    private User user;

    private OrderItemStatus status;

    private int id;

    private int id2;

    private String password;

    private JavaMailSender sender;

    private NotificationService notificationService;

    @BeforeEach
    void setUp() {
        user = new User();
        status = OrderItemStatus.ACCEPTED;
        id = 5;
        id2 = 7;
        sender = mock(JavaMailSender.class);
        notificationService = new NotificationService(sender);
        password = "Password123";
    }

    @Test
    void sendEmailWhenOrderStatusChange() {
        notificationService.sendEmailWhenOrderStatusChange(user, status, id);
        verify(sender).send(any(SimpleMailMessage.class));
    }

    @Test
    void sendNewPassword() {
        notificationService.sendNewPassword(user, password);
        verify(sender).send(any(SimpleMailMessage.class));
    }
}