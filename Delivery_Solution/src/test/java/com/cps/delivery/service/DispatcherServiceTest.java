package com.cps.delivery.service;

import com.cps.delivery.domain.Admin;
import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Dispatcher;
import com.cps.delivery.domain.User;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.AdminRepository;
import com.cps.delivery.repository.CourierRepository;
import com.cps.delivery.repository.DispatcherRepository;
import com.cps.delivery.repository.UserRepository;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.aspectj.weaver.ast.Not;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DispatcherServiceTest {

    private DispatcherRepository dispatcherRepository;

    private DispatcherService dispatcherService;

    private Dispatcher dispatcher;

    private int id;

    private String email;

    private String password;

    @BeforeEach
    void setUp() {
        dispatcher = new Dispatcher();
        dispatcherRepository = mock(DispatcherRepository.class);
        dispatcherService = new DispatcherService(dispatcherRepository);
        id = 5;
        email = "aaaaa@gmail.com";
        password = "Password123";
    }

    @Test
    void insertOrUpdate() {
        when(dispatcherRepository.save(dispatcher)).thenReturn(dispatcher);
        dispatcherService.insertOrUpdate(dispatcher);
        verify(dispatcherRepository).save(dispatcher);
    }

    @Test
    void findAllDispatchersWhenDispatchersNotExist() {
        when(dispatcherRepository.findAll()).thenReturn(null);
        Assert.assertNull(null, dispatcherService.findAllDispatchers());
    }

    @Test
    void findAllDispatchersWhenDispatchersExist() {
        List<Dispatcher> dispatcherList = new ArrayList<>();
        dispatcherList.add(new Dispatcher());
        dispatcherList.add(new Dispatcher());
        dispatcherList.add(new Dispatcher());
        when(dispatcherRepository.findAll()).thenReturn(dispatcherList);
        Iterable<Dispatcher> dispatchers = dispatcherService.findAllDispatchers();
        assertEquals(dispatcherList, dispatchers);
    }

    @Test
    void deleteDispatcherWhenIdNotExist() {
        when(dispatcherRepository.findById(id)).thenReturn(null);
        dispatcher.setId(id);
        assertThrows(CustomErrorException.class, () -> dispatcherService.deleteDispatcher(id));
    }

    @Test
    void deleteDispatcherWhenIdExists() {
        dispatcher.setId(id);
        when(dispatcherRepository.findById(id)).thenReturn(dispatcher);
        dispatcherService.deleteDispatcher(id);
        verify(dispatcherRepository).deleteById(id);
    }

    @Test
    void findByIdWhenIdNotExist() {
        when(dispatcherRepository.findById(id)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> dispatcherService.findById(id));
    }

    @Test
    void findByIdWhenIdExists() {
        dispatcher.setId(id);
        when(dispatcherRepository.findById(id)).thenReturn(dispatcher);
        Assert.assertEquals(id, dispatcherService.findById(id).getId());
    }

    @Test
    void findDispatcherByEmailAndPassword() {
        dispatcher.setEmail(email);
        dispatcher.setPassword(password);
        when(dispatcherRepository.findDispatcherByEmailAndPassword(email, password)).thenReturn(dispatcher);
        Assert.assertEquals(email, dispatcher.getEmail());
        Assert.assertEquals(password, dispatcher.getPassword());
    }

    @Test
    void findDispatcherByEmail() {
        dispatcher.setEmail(email);
        when(dispatcherRepository.findDispatcherByEmail(email)).thenReturn(dispatcher);
        Assert.assertEquals(email, dispatcher.getEmail());
    }
}