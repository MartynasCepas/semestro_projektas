package com.cps.delivery.service;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.OrderItem;
import com.cps.delivery.domain.Rejection;
import com.cps.delivery.domain.User;
import com.cps.delivery.repository.CourierRepository;
import com.cps.delivery.repository.OrderRepository;
import com.cps.delivery.repository.RejectionRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RejectionServiceTest {

    private OrderRepository orderRepository;
    private CourierRepository courierRepository;
    private RejectionRepository rejectionRepository;
    private RejectionService rejectionService;

    private int id;
    private int id2;

    private Rejection rejection;
    private Courier courier;
    private OrderItem orderItem;

    @BeforeEach
    void setUp() {
        orderRepository = mock(OrderRepository.class);
        courierRepository = mock(CourierRepository.class);
        rejectionRepository = mock(RejectionRepository.class);
        rejectionService = new RejectionService(orderRepository, courierRepository, rejectionRepository);
        id = 5;
        id2 = 6;
        rejection = new Rejection();
        courier = new Courier();
        orderItem = new OrderItem();
    }

    @Test
    void getCourierRejectionsWhenRejectionsNotExist() {
        when(rejectionRepository.findRejectionsByRejectionsForCourierId(id)).thenReturn(null);
        Assert.assertNull(null, rejectionService.getCourierRejections(id));
    }

    @Test
    void getCourierRejectionsWhenRejectionsExist() {
        List<Rejection> rejectionsList = new ArrayList<>();
        rejectionsList.add(new Rejection());
        rejectionsList.add(new Rejection());
        rejectionsList.add(new Rejection());
        when(rejectionRepository.findRejectionsByRejectionsForCourierId(id)).thenReturn(rejectionsList);
        Iterable<Rejection> rejections = rejectionService.getCourierRejections(id);
        assertEquals(rejectionsList, rejections);
    }

    @Test
    void getAllRejectionsWhenRejectionsExist() {
        List<Rejection> rejectionsList = new ArrayList<>();
        rejectionsList.add(new Rejection());
        rejectionsList.add(new Rejection());
        rejectionsList.add(new Rejection());
        when(rejectionRepository.findAll()).thenReturn(rejectionsList);
        Iterable<Rejection> rejections = rejectionService.getAllRejections();
        assertEquals(rejectionsList, rejections);
    }

    @Test
    void getAllRejectionsWhenRejectionsNotExist() {
        when(rejectionRepository.findAll()).thenReturn(null);
        Assert.assertNull(null, rejectionService.getAllRejections());
    }

    @Test
    void rejectOrder() {
        courier.setId(id);
        orderItem.setId(id2);
        when(courierRepository.findById(id)).thenReturn(courier);
        when(orderRepository.findById(id2)).thenReturn(orderItem);
        rejectionService.rejectOrder(id, id2);
        verify(rejectionRepository).save(any(Rejection.class));
    }
}