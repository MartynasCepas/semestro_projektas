package com.cps.delivery.service;

import com.cps.delivery.domain.Courier;
import com.cps.delivery.domain.Rejection;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.CourierRepository;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CourierServiceTest {

    private CourierRepository repository;
    private Courier courier;
    private int id;
    private CourierService courierService;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @BeforeEach
    void setUp() {
        courier = new Courier();
        repository = mock(CourierRepository.class);
        id = 5;
        courierService = new CourierService(repository);
    }

    @Test
    void insertOrUpdate() {
        when(repository.save(courier)).thenReturn(courier);
        courierService.insertOrUpdate(courier);
        verify(repository).save(courier);
    }

    @Test
    void deleteCourierByIdWhichDoesNotExist() throws CustomErrorException {
        when(repository.findById(id)).thenReturn(null);
        courier.setId(id);
        assertThrows(CustomErrorException.class, () -> courierService.deleteCourierById(id));
    }

    @Test
    void deleteCourierByIdWhichExists(){
        courier.setId(id);
        when(repository.findById(id)).thenReturn(courier);
        courierService.deleteCourierById(id);
        verify(repository).deleteById(id);
    }

    @Test//(expected = CustomErrorException.class)
    void findByIdWhenIdDoesNotExist() throws CustomErrorException {
        when(repository.findById(id)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> courierService.findById(id));
    }

    @Test
    void findByIdWhenIdExists() {
        courier.setId(id);
        when(repository.findById(id)).thenReturn(courier);
        Assert.assertEquals(id, courierService.findById(id).getId());
    }

    @Test
    void findAllCouriersWhenCouriersDoNotExist() {
        when(repository.findAll()).thenReturn(null);
        Assert.assertNull(null, courierService.findAllCouriers());
    }

    @Test
    void findAllCouriersWhenCouriersExist(){
        List<Courier> courierList = new ArrayList<>();
        courierList.add(new Courier());
        courierList.add(new Courier());
        courierList.add(new Courier());
        when(repository.findAll()).thenReturn(courierList);
        Iterable<Courier> couriers = courierService.findAllCouriers();
        assertEquals(courierList, couriers);
    }

    @Test
    void findCouriersRejectionsWhenTheyExist() {
        courier.setId(id);
        List<Rejection> rejectionList = new ArrayList<>();
        rejectionList.add(new Rejection());
        rejectionList.add(new Rejection());
        rejectionList.add(new Rejection());
        when(repository.findCourierById(id)).thenReturn(rejectionList);
        Iterable<Rejection> rejections = courierService.findCouriersRejections(id);
        assertEquals(rejectionList, rejections);
    }

    @Test
    void findCouriersRejectionsWhenTheyDoNotExist() {
        Iterable<Rejection> rejections = courierService.findCouriersRejections(id);
        when(repository.findCourierById(id)).thenReturn(null);
        Assert.assertNull(null, courierService.findCouriersRejections(id));
    }
}