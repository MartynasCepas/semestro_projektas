package com.cps.delivery.service;

import com.cps.delivery.domain.*;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.*;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.aspectj.weaver.ast.Not;
import org.aspectj.weaver.ast.Or;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderServiceTest {

    private OrderRepository orderRepository;

    private DispatcherRepository dispatcherRepository;

    private CourierRepository courierRepository;

    private RejectionRepository rejectionRepository;

    private UserRepository userRepository;

    private AdminRepository adminRepository;

    private NotificationService notificationService;

    private OrderItem orderItem;

    private OrderService orderService;

    private Dispatcher dispatcher;

    private Courier courier;

    private User user;

    private int id;

    private int id2;

    @BeforeEach
    void setUp() {
        orderItem = new OrderItem();
        dispatcher = new Dispatcher();
        user = new User();
        orderRepository = mock(OrderRepository.class);
        dispatcherRepository = mock(DispatcherRepository.class);
        courierRepository = mock(CourierRepository.class);
        rejectionRepository = mock(RejectionRepository.class);
        userRepository = mock(UserRepository.class);
        adminRepository = mock(AdminRepository.class);
        notificationService = mock(NotificationService.class);
        orderService = new OrderService(orderRepository, dispatcherRepository, courierRepository, rejectionRepository, userRepository, adminRepository, notificationService);
        id = 5;
        id2 = 7;
    }

    @Test
    void createOrderItemWhenDispatcherNotExist() {
        dispatcher.setId(id);
        when(dispatcherRepository.findById(dispatcher.getId())).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> orderService.createOrderItem(orderItem, id));
    }

    @Test
    void createOrderItemWhenDispatcherExists() {
        dispatcher.setId(id);
        when(dispatcherRepository.findById(dispatcher.getId())).thenReturn(dispatcher);
        when(orderRepository.save(orderItem)).thenReturn(orderItem);
        orderService.createOrderItem(orderItem, id);
        verify(orderRepository).save(orderItem);
    }

    @Test
    void updateOrder() {
        orderItem.setId(id);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        //Assert.assertEquals(id, userService.findById(id).getId());
        orderService.updateOrder(orderItem);
        verify(orderRepository).save(orderItem);
    }

    @Test
    void setCourierAndSaveWhenOrderItemNotExist() {
        courier = new Courier();
        orderItem.setId(id);
        courier.setId(id2);
        when(orderRepository.findById(id)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> orderService.setCourierAndSave(id, id2));
    }

    @Test
    void setCourierAndSaveWhenOrderItemExistsButCourierNot() {
        courier = new Courier();
        orderItem.setId(id);
        courier.setId(id2);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        when(courierRepository.findById(id2)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> orderService.setCourierAndSave(id, id2));
    }

    @Test
    void setCourierAndSaveWhenOrderItemAndCourierExist() {
        courier = new Courier();
        orderItem.setId(id);
        courier.setId(id2);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        when(courierRepository.findById(id2)).thenReturn(courier);
        orderService.setCourierAndSave(id2, id);
        verify(orderRepository).save(orderItem);
    }

    @Test
    void convertStatusToObjectWhenStatusNotExist() {
        String status = "NotExist";
        Assert.assertNull(null, orderService.convertStatusToObject(status));
    }

    @Test
    void convertStatusToObjectWhenStatusExists() {
        String status = "Accepted";
        Assert.assertNotNull(status, orderService.convertStatusToObject(status));
    }

    @Test
    void changeStatusWhenOrderNotExist() {
        when(orderRepository.findById(id)).thenReturn(null);
        assertThrows(NullPointerException.class, () -> orderService.changeStatus(id, any(OrderItemStatus.class)));
    }

    @Test
    void changeStatusWhenOrderExistsAndCurrentStatusCreatedAndNewAccepted() {
        OrderItemStatus newStatus = OrderItemStatus.ACCEPTED;
        orderItem.setStatus(OrderItemStatus.CREATED);
        orderItem.setDispatcher(dispatcher);
        user.setId(id2);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        when(userRepository.findById(orderItem.getDispatcher().getId())).thenReturn(user);
        orderService.changeStatus(id, newStatus);
        verify(notificationService).sendEmailWhenOrderStatusChange(user, newStatus, id);
    }

    @Test
    void changeStatusWhenCurrentStatusCreatedAndNewRejected() {
        OrderItemStatus newStatus = OrderItemStatus.REJECTED;
        orderItem.setStatus(OrderItemStatus.CREATED);
        orderItem.setDispatcher(dispatcher);
        user.setId(id2);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        when(userRepository.findById(orderItem.getDispatcher().getId())).thenReturn(user);
        orderService.changeStatus(id, newStatus);
        verify(orderRepository).save(orderItem);
    }

    @Test
    void changeStatusWhenCurrentStatusCreatedAndNewIncorrect() {
        OrderItemStatus newStatus = OrderItemStatus.IN_TRANSIT;
        orderItem.setId(id);
        orderRepository.save(orderItem);
        orderItem.setStatus(OrderItemStatus.CREATED);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        assertThrows(CustomErrorException.class, () -> orderService.changeStatus(id, newStatus));
    }

    @Test
    void changeStatusWhenCurrentStatusAcceptedAndNewInTransit() {
        OrderItemStatus newStatus = OrderItemStatus.IN_TRANSIT;
        orderItem.setId(id);
        orderItem.setDispatcher(dispatcher);
        orderItem.setStatus(OrderItemStatus.ACCEPTED);
        user.setId(id2);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        when(userRepository.findById(orderItem.getDispatcher().getId())).thenReturn(user);
        orderService.changeStatus(id, newStatus);
        verify(orderRepository).save(orderItem);
        verify(notificationService).sendEmailWhenOrderStatusChange(user, newStatus, id);
    }

    @Test
    void changeStatusWhenCurrentStatusAcceptedAndNewIncorrect() {
        OrderItemStatus newStatus = OrderItemStatus.REJECTED;
        orderItem.setId(id);
        orderRepository.save(orderItem);
        orderItem.setStatus(OrderItemStatus.ACCEPTED);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        assertThrows(CustomErrorException.class, () -> orderService.changeStatus(id, newStatus));
    }

    @Test
    void changeStatusWhenCurrentStatusInTransitAndNewDelivered() {
        OrderItemStatus newStatus = OrderItemStatus.DELIVERED;
        orderItem.setId(id);
        orderRepository.save(orderItem);
        orderItem.setStatus(OrderItemStatus.IN_TRANSIT);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        Assert.assertNotNull("Changed to delivered", true);
    }

    @Test
    void changeStatusWhenCurrentStatusInTransitAndNewFailedToDeliver() {
        OrderItemStatus newStatus = OrderItemStatus.FAILED_TO_DELIVER;
        orderItem.setId(id);
        orderItem.setDispatcher(dispatcher);
        orderItem.setStatus(OrderItemStatus.IN_TRANSIT);
        user.setId(id2);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        when(userRepository.findById(orderItem.getDispatcher().getId())).thenReturn(user);
        orderService.changeStatus(id, newStatus);
        verify(orderRepository).save(orderItem);
        verify(notificationService).sendEmailWhenOrderStatusChange(user, newStatus, id);
    }

    @Test
    void findAllOrdersWhenOrdersNotExist() {
        when(orderRepository.findAll()).thenReturn(null);
        Assert.assertNull(null, orderService.findAllOrders());
    }

    @Test
    void findAllOrdersWhenOrdersExist() {
        List<OrderItem> orderItemList = new ArrayList<>();
        orderItemList.add(new OrderItem());
        orderItemList.add(new OrderItem());
        orderItemList.add(new OrderItem());
        when(orderRepository.findAll()).thenReturn(orderItemList);
        Iterable<OrderItem> orderItems = orderService.findAllOrders();
        assertEquals(orderItemList, orderItems);
    }

    @Test
    void findOrdersWithStatusCreatedWhenTheyNotExist() {
        when(orderRepository.findOrderItemsByStatus(OrderItemStatus.CREATED)).thenReturn(null);
        Assert.assertNull(null, orderService.findOrdersWithStatusCreated());
    }

    @Test
    void findOrdersWithStatusCreatedWhenTheyExist() {
        List<OrderItem> orderItemList = new ArrayList<>();
        OrderItem orderItem1 = new OrderItem();
        OrderItem orderItem2 = new OrderItem();
        OrderItem orderItem3 = new OrderItem();
        orderItem1.setStatus(OrderItemStatus.CREATED);
        orderItem2.setStatus(OrderItemStatus.CREATED);
        orderItem3.setStatus(OrderItemStatus.CREATED);
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        orderItemList.add(orderItem3);
        when(orderRepository.findOrderItemsByStatus(OrderItemStatus.CREATED)).thenReturn(orderItemList);
        Iterable<OrderItem> orderItems = orderService.findOrdersWithStatusCreated();
        assertEquals(orderItemList, orderItems);
    }

    @Test
    void findUniqueOrdersWhenAllOrSomeAreNotUnique() {
        List<OrderItem> orders = new ArrayList<>();
        List<Rejection> rejectionsForCourier = new ArrayList<>();
        orders.add(new OrderItem(10));
        orders.add(new OrderItem(20));
        orders.add(new OrderItem(30));
        Courier courier = new Courier();
        Courier courier2 = new Courier();
        Courier courier3 = new Courier();
        courier.setId(id);
        courier2.setId(6);
        courier3.setId(7);
        rejectionsForCourier.add(new Rejection(20, new OrderItem(10), courier));
        rejectionsForCourier.add(new Rejection(40, new OrderItem(20), courier2));
        rejectionsForCourier.add(new Rejection(70, new OrderItem(35), courier3));
        List<OrderItem> unique = new ArrayList<>();
        List<OrderItem> ordersFromRejections = new ArrayList<>();
        List<Integer> idsOfOrders = new ArrayList<>();
        List<Integer> idsOfRejections = new ArrayList<>();
        for(Rejection i : rejectionsForCourier){
           ordersFromRejections.add(i.getRejectedItems());
        }
        for(OrderItem i : orders){
            idsOfOrders.add(i.getId());
        }
        for(OrderItem i : ordersFromRejections){
            idsOfRejections.add(i.getId());
        }
        for(int i=0;i<idsOfOrders.size(); i++){
            if(idsOfRejections.contains(idsOfOrders.get(i))){
                continue;
            }
            else{
                for(OrderItem j : orders){
                    if(j.getId() == idsOfOrders.get(i)){
                        unique.add(j);
                    }
                }
            }
        }
        when(orderRepository.findAll()).thenReturn(orders);
        when(rejectionRepository.findRejectionsByRejectionsForCourierId(id)).thenReturn(rejectionsForCourier);
        int size = 0;
        Iterable<OrderItem> foundUnique = orderService.findUniqueOrders(id);
        for(Object i : foundUnique){
            size++;
        }
        Assert.assertEquals(unique.size(),  size);
    }

    @Test
    void findUniqueOrdersWhenAllAreUnique() { // TODO
        List<OrderItem> orders = new ArrayList<>();
        List<Rejection> rejectionsForCourier = new ArrayList<>();
        orders.add(new OrderItem(10));
        orders.add(new OrderItem(20));
        Courier courier = new Courier();
        Courier courier2 = new Courier();
        courier.setId(id);
        courier2.setId(6);
        rejectionsForCourier.add(new Rejection(20, new OrderItem(10), courier));
        rejectionsForCourier.add(new Rejection(40, new OrderItem(20), courier2));
        when(orderRepository.findAll()).thenReturn(orders);
        when(rejectionRepository.findRejectionsByRejectionsForCourierId(id)).thenReturn(rejectionsForCourier);
        int size = 0;
        Iterable<OrderItem> foundUnique = orderService.findUniqueOrders(id);
        for(Object i : foundUnique){
            size++;
        }
        Assert.assertEquals(0,  size);
    }

    @Test
    void deleteOrderWhenOrderNotExist() {
        when(orderRepository.findById(id)).thenReturn(null);
        orderItem.setId(id);
        assertThrows(CustomErrorException.class, () -> orderService.deleteOrder(id));
    }

    @Test
    void deleteOrderWhichExists() {
        orderItem.setId(id);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        orderService.deleteOrder(id);
        verify(orderRepository).deleteById(id);
    }

    @Test
    void findByIdWhichExists() {
        orderItem.setId(id);
        when(orderRepository.findById(id)).thenReturn(orderItem);
        Assert.assertEquals(id, orderService.findById(id).getId());
    }

    @Test
    void findByIdWhichDoesNotExist() {
        int id = 5;
        when(orderRepository.findById(id)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> orderService.findById(id));
    }

    @Test
    void getUserOrdersWhenOrdersNotExist() {
        when(userRepository.findById(id)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> orderService.getUserOrders(id));
    }

    @Test
    void getUserOrdersWhenOrdersExistAndUserIsDispatcher() {
        List<OrderItem> orderItems = new ArrayList<>();
        user.setId(id);
        user.setType("dispatcher");
        orderItems.add(new OrderItem(10));
        orderItems.add(new OrderItem(20));
        orderItems.add(new OrderItem(30));
        when(userRepository.findById(id)).thenReturn(user);
        when(orderRepository.findByDispatcherId(id)).thenReturn(orderItems);
        List<OrderItem> orderItemsList = orderService.getUserOrders(id);
        assertEquals(orderItems, orderItemsList);
    }

    @Test
    void getCourierOrdersWhenOrdersExist() {
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(new OrderItem(10));
        orderItems.add(new OrderItem(20));
        orderItems.add(new OrderItem(30));
        when(orderRepository.findByCourierId(id)).thenReturn(orderItems);
        List<OrderItem> orderItemsList = orderService.getCourierOrders(id);
        assertEquals(orderItems, orderItemsList);
    }
    @Test
    void getCourierOrdersWhenOrdersNotExist() {
        when(orderRepository.findByCourierId(id)).thenReturn(null);
        assertThrows(NullPointerException.class, () -> orderService.getCourierOrders(id));
    }
}