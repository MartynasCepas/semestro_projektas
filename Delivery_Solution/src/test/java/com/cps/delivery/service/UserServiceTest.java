package com.cps.delivery.service;

import com.cps.delivery.domain.*;
import com.cps.delivery.exceptions.CustomErrorException;
import com.cps.delivery.repository.AdminRepository;
import com.cps.delivery.repository.CourierRepository;
import com.cps.delivery.repository.DispatcherRepository;
import com.cps.delivery.repository.UserRepository;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.aspectj.weaver.ast.Not;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest {

    private CourierRepository courierRepository;

    private DispatcherRepository dispatcherRepository;

    private UserRepository userRepository;

    private AdminRepository adminRepository;

    private NotificationService notificationService;

    private UserService userService;

    private User user;

    @BeforeEach
    void setUp() {
        user = new User();
        userRepository = mock(UserRepository.class);
        courierRepository = mock(CourierRepository.class);
        dispatcherRepository = mock(DispatcherRepository.class);
        adminRepository = mock(AdminRepository.class);
        notificationService = mock(NotificationService.class);
        userService = new UserService(dispatcherRepository, courierRepository, userRepository, adminRepository, notificationService);
    }

    @Test
    void findUserWhenLoginIfEmailDoesNotExist() {
        String email = "aaaa@gmail.com";
        String pass = "123";
        when(userRepository.findByEmail(email)).thenReturn(null);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.findUserWhenLogin(email, pass);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, var.getStatusCode());
    }

    @Test
    void findUserWhenLoginIfEmailExists() {
        String email = "aaaa@gmail.com";
        String pass = "123";
        when(userRepository.findByEmail(email)).thenReturn(new User());
        when(userRepository.findByEmailAndPassword(email, pass)).thenReturn(null);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.findUserWhenLogin(email, pass);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, var.getStatusCode());
    }

    @Test
    void findUserWhenLoginIfEmailAndPassExist() {
        String email = "aaaa@gmail.com";
        String pass = "123";
        when(userRepository.findByEmail(email)).thenReturn(user);
        when(userRepository.findByEmailAndPassword(email, pass)).thenReturn(user);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.findUserWhenLogin(email, pass);
        Assert.assertEquals(HttpStatus.OK, var.getStatusCode());
    }

    @Test
    void findByEmailWhenItExists() {
        String email = "aaaa@gmail.com";
        user.setEmail(email);
        when(userRepository.findByEmail(email)).thenReturn(user);
        Assert.assertEquals(email, userService.findByEmail(email).getEmail());
    }

    @Test
    void findByEmailWhenItDoesNotExist() {
        String email = "aaaa@gmail.com";
        user.setEmail(email);
        when(userRepository.findByEmail(email)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> userService.findByEmail(email));
    }

    @Test
    void registerUserWhenEmailIsTaken() {
        String email = "aaaa@gmail.com";
        user.setEmail(email);
        when(userRepository.findByEmail(email)).thenReturn(user);
        assertThrows(CustomErrorException.class, () -> userService.registerUser(user));
    }

    @Test
    void registerUserWhenEmailIsAvailableCourier() {
        String email = "aaaa@gmail.com";
        user.setEmail(email);
        user.setType("courier");
        when(userRepository.findByEmail(email)).thenReturn(null);
        Courier courier = new Courier();
        when(courierRepository.save(courier)).thenReturn(courier);
        userService.registerUser(user);
        verify(courierRepository).save(any(Courier.class));
    }

    @Test
    void registerUserWhenEmailIsAvailableDispatcher() {
        String email = "aaaa@gmail.com";
        user.setEmail(email);
        user.setType("dispatcher");
        when(userRepository.findByEmail(email)).thenReturn(null);
        Dispatcher dispatcher = new Dispatcher();
        when(dispatcherRepository.save(dispatcher)).thenReturn(dispatcher);
        userService.registerUser(user);
        verify(dispatcherRepository).save(any(Dispatcher.class));
    }

    @Test
    void registerUserWhenEmailIsAvailableAvailable() {
        String email = "aaaa@gmail.com";
        user.setEmail(email);
        user.setType("admin");
        when(userRepository.findByEmail(email)).thenReturn(null);
        Admin admin = new Admin();
        when(adminRepository.save(admin)).thenReturn(admin);
        userService.registerUser(user);
        verify(adminRepository).save(any(Admin.class));
    }

    @Test
    void changePasswordWhenUserDoesNotExist() {
        int id = 5;
        String oldPassword = "Password1";
        String newPassword = "Password123";
        user.setPassword(oldPassword);
        when(userRepository.findById(5)).thenReturn(null);
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        node.put("oldPassword", oldPassword);
        node.put("newPassword", newPassword);
        assertThrows(CustomErrorException.class, () -> userService.changePassword(node, id));
    }

    @Test
    void changePasswordWhenUserExist() {
        int id = 5;
        String oldPassword = "Password1";
        String oldPasswordIncorrect = "Pass";
        String newPassword = "Password123";
        user.setPassword(oldPassword);
        when(userRepository.findById(5)).thenReturn(user);
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        node.put("oldPassword", oldPasswordIncorrect);
        node.put("newPassword", newPassword);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.changePassword(node, id);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, var.getStatusCode());
        Assert.assertEquals("Old password was incorrect", var.getBody().get("oldPassword"));
    }

    @Test
    void changePasswordWhenNewPasswordIsEmpty() {
        int id = 5;
        String oldPassword = "Password1";
        String oldPasswordIncorrect = "Pass";
        String newPassword = "Password123";
        user.setPassword(oldPassword);
        when(userRepository.findById(5)).thenReturn(user);
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        node.put("oldPassword", oldPassword);
        node.put("newPassword", "");
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.changePassword(node, id);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, var.getStatusCode());
        Assert.assertEquals("Please fix your password", var.getBody().get("newPassword"));
    }

    @Test
    void changePasswordWhenNewPasswordIsOldPassword() {
        int id = 5;
        String oldPassword = "Password1";
        String oldPasswordIncorrect = "Pass";
        String newPassword = "Password123";
        user.setPassword(oldPassword);
        when(userRepository.findById(5)).thenReturn(user);
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        node.put("oldPassword", oldPassword);
        node.put("newPassword", oldPassword);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.changePassword(node, id);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, var.getStatusCode());
        Assert.assertEquals("Old password is the same as new password", var.getBody().get("newPassword"));
    }

    @Test
    void changePasswordWhenPasswordsOK() {
        int id = 5;
        String oldPassword = "Password1";
        String oldPasswordIncorrect = "Pass";
        String newPassword = "Password123";
        user.setPassword(oldPassword);
        when(userRepository.findById(5)).thenReturn(user);
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        node.put("oldPassword", oldPassword);
        node.put("newPassword", newPassword);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.changePassword(node, id);
        Assert.assertEquals(HttpStatus.OK, var.getStatusCode());
        Assert.assertEquals("Password was changed successfully", var.getBody());
    }


    @Test
    void resetPasswordWhenUserNotExist() {
        String email = "aaaaa@gmail.com";
        user.setEmail(email);
        when(userRepository.findByEmail(email)).thenReturn(null);
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        node.put("email", email);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.resetPassword(node);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, var.getStatusCode());
        Assert.assertEquals("There is no user with given email address", var.getBody().get("email"));
    }

    @Test
    void resetPasswordWhenUserExists() {
        String email = "aaaaa@gmail.com";
        user.setEmail(email);
        when(userRepository.findByEmail(email)).thenReturn(user);
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        node.put("email", email);
        ResponseEntity<Map<String, String>> var = (ResponseEntity<Map<String, String>>) userService.resetPassword(node);
        Assert.assertEquals(HttpStatus.OK, var.getStatusCode());
        Assert.assertEquals("We have sent a new password to specified email address", var.getBody());
    }

    @Test
    void findAllUsersWhenUsersDoNotExist() {
        when(userRepository.findAll()).thenReturn(null);
        Assert.assertNull(null, userService.findAllUsers());
    }

    @Test
    void findAllUsersWhenUsersExist(){
        List<User> userList = new ArrayList<>();
        userList.add(new User());
        userList.add(new User());
        userList.add(new User());
        when(userRepository.findAll()).thenReturn(userList);
        Iterable<User> users = userService.findAllUsers();
        assertEquals(userList, users);
    }

    @Test
    void insertOrUpdate() {
        when(userRepository.save(user)).thenReturn(user);
        userService.insertOrUpdate(user);
        verify(userRepository).save(user);
    }

    @Test
    void updateUser() {
        int id = 5;
        user.setId(id);
        when(userRepository.findById(id)).thenReturn(user);
        userService.updateUser(user);
        verify(userRepository).save(user);
    }

    @Test
    void deleteUserByIdWhichNotExist() {
        int id = 5;
        when(userRepository.findById(id)).thenReturn(null);
        user.setId(id);
        assertThrows(CustomErrorException.class, () -> userService.deleteUserById(id));
    }

    @Test
    void deleteUserByIdWhichExists() {
        int id = 5;
        user.setId(id);
        when(userRepository.findById(id)).thenReturn(user);
        userService.deleteUserById(id);
        verify(userRepository).deleteById(id);
    }

    @Test
    void findByIdWhichDoesNotExist() {
        int id = 5;
        when(userRepository.findById(id)).thenReturn(null);
        assertThrows(CustomErrorException.class, () -> userService.findById(id));
    }

    @Test
    void findByIdWhenIdExists() {
        int id = 5;
        user.setId(id);
        when(userRepository.findById(id)).thenReturn(user);
        Assert.assertEquals(id, userService.findById(id).getId());
    }
}