import axios from "axios";
import { GET_USERS, DELETE_USER, GET_USER, GET_ERRORS } from "./types";

export const getUsers = () => async (dispatch) => {
  try {
    const res = await axios.get("/admin/users/all");
    dispatch({
      type: GET_USERS,
      payload: res.data,
    });
  } catch (e) {
    console.log(e);
  }
};

export const deleteUser = (id) => async (dispatch) => {
  if (
    window.confirm(
      "Are you sure? This will delete the user and all the data related to it"
    )
  ) {
    await axios.delete(`/admin/user/${id}`);
    dispatch({
      type: DELETE_USER,
      payload: id,
    });
  }
};

export const getUser = (id, history) => async (dispatch) => {
  try {
    const res = await axios.get(`/admin/user/${id}`);
    dispatch({
      type: GET_USER,
      payload: res.data,
    });
  } catch (e) {
    history.push("/admin/users");
  }
};

export const updateUser = (user, history) => async (dispatch) => {
  try {
    const res = await axios.put("/admin/user/update", user);
    console.log(res);
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
    history.push("/admin/users");
  } catch (e) {
    dispatch({
      type: GET_ERRORS,
      payload: e.response.data,
    });
  }
};
