import axios from "axios";
import { GET_ERRORS, LOGIN_USER } from "./types";

export const loginUser = (userLogin, history) => async (dispatch) => {
  try {
    const res = await axios.post("/user/login", userLogin);
    console.log(res);
    history.push("/dashboard/myorders");
    history.go();
    dispatch({
      type: LOGIN_USER,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    });
  }
};
