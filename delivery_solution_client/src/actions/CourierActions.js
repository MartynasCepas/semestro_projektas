import axios from "axios";
import { GET_ERRORS, LOGIN_USER } from "./types";

export const createCourier = (courier, history) => async (dispatch) => {
  try {
    console.log(courier);
    const res = await axios.post("/user/register", courier);
    history.push("/login");
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    });
  }
};

export const acceptOrder = (courierId, orderId) => async (dispatch) => {
  try {
    const res = await axios.post(`/order/${courierId}/${orderId}`);
    console.log(res);
  } catch (e) {
    console.log(e);
  }
};

export const rejectOrder = (courierId, orderId) => async (dispatch) => {
  try {
    const res = await axios.post(`/rejected/${courierId}/${orderId}`);
    console.log(res);
    window.location.reload(false);
  } catch (e) {
    console.log(e);
  }
};
