import axios from "axios";
import { GET_ERRORS, GET_ORDERS, GET_ORDER, DELETE_ORDER } from "./types";

export const createOrder = (id, order, history) => async (dispatch) => {
  try {
    const res = await axios.post(`/order/user/${id}`, order);
    console.log(res);
    history.push("/dashboard/myorders");
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    });
  }
};

export const updateOrder = (order, history) => async (dispatch) => {
  try {
    const res = await axios.put("/order/update", order);
    history.push("/dashboard/myorders");
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    });
  }
};

export const getOrders = (userId, type, request) => async (dispatch) => {
  try {
    let res;
    if (type === "dispatcher") {
      res = await axios.get(`/order/user/${userId}`);
    } else if (type === "courier" && request === "choose") {
      res = await axios.get(`/order/available/${userId}`);
    } else if (type === "courier" && request === "myorders") {
      res = await axios.get(`/order/courier/${userId}/all`);
    } else if (type === "admin") {
      res = await axios.get(`/order/all`);
    }
    dispatch({
      type: GET_ORDERS,
      payload: res.data,
    });
  } catch (e) {
    console.log(e);
  }
};

export const getOrder = (id, history) => async (dispatch) => {
  try {
    const res = await axios.get(`/order/${id}`);
    dispatch({
      type: GET_ORDER,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};

export const deleteOrder = (id) => async (dispatch) => {
  if (
    window.confirm(
      "Are you sure? This will delete the order and all the data related to it"
    )
  ) {
    await axios.delete(`/order/${id}`);
    dispatch({
      type: DELETE_ORDER,
      payload: id,
    });
  }
};

export const changeOrderStatus = (orderId, newStatus) => async (dispatch) => {
  try {
    const res = await axios.post(`/order/status/${orderId}/${newStatus}`);
    window.location.reload(false);
  } catch (e) {
    console.log(e);
  }
};

export const filterOrders = (type, userId, value) => async (dispatch) => {
  try {
    const res = await axios.get(`/order/user/${type}/${userId}/${value}`);
    dispatch({
      type: GET_ORDERS,
      payload: res.data,
    });
  } catch (e) {
    console.log(e);
  }
};

export const sortOrders = (userId, type) => async (dispatch) => {
  try {
    const res = await axios.get(`/order/user/${type}/${userId}`);
    dispatch({
      type: GET_ORDERS,
      payload: res.data,
    });
  } catch (e) {
    console.log(e);
  }
};
