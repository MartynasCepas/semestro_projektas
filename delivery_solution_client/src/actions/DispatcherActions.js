import axios from "axios";
import { GET_ERRORS, LOGIN_USER } from "./types";

export const createDispatcher = (dispatcher, history) => async (dispatch) => {
  try {
    const res = await axios.post("/user/register", dispatcher);
    history.push("/login");
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    });
  }
};
