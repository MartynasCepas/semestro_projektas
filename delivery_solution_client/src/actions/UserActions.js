import axios from "axios";
import { GET_ERRORS } from "./types";

export const changePassword = (id, data) => async (dispatch) => {
  try {
    const res = await axios.put(`/user/change/${id}`, data);
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
  } catch (e) {
    console.log(e);
    dispatch({
      type: GET_ERRORS,
      payload: e.response.data,
    });
  }
};

export const resetPassword = (email, history) => async (dispatch) => {
  try {
    const res = await axios.put(`/user/resetPassword`, email);
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
    history.push("login");
  } catch (e) {
    console.log(e);
    dispatch({
      type: GET_ERRORS,
      payload: e.response.data,
    });
  }
};
