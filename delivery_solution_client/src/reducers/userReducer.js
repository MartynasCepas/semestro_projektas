import {
  LOGIN_USER,
  LOGOUT_USER,
  GET_USERS,
  DELETE_USER,
  GET_USER,
} from "../actions/types";

const initialState = {
  user: {},
  users: {},
  editUser: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state,
        user: action.payload,
      };
    case LOGOUT_USER:
      return {
        initialState,
      };
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
      };
    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.id !== action.payload),
      };
    case GET_USER:
      return {
        ...state,
        editUser: action.payload,
      };
    default:
      return state;
  }
}
