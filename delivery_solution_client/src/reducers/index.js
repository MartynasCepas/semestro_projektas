import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import orderReducer from "./orderReducer";
import userReducer from "./userReducer";

const rootReducer = combineReducers({
  errors: errorReducer,
  order: orderReducer,
  user: userReducer,
  users: userReducer,
});

export default (state, action) =>
  rootReducer(action.type === "LOGOUT_USER" ? undefined : state, action);
