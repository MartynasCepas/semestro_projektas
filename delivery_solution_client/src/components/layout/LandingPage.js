import React, { Component } from "react";
import "../../css/main.css";

class LandingPage extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <body className="body-main">
        <section className="container-main">
          <img
            src={require("./../../images/delivery1.jpg")}
            alt="server graphic"
            className="server"
            height="600px"
            width="600px"
          ></img>
          <h1 class="h1index">Best delivery system for you!</h1>
        </section>

        <div className="blue-container">
          <div className="container-main">
            <ul>
              <li>
                <img
                  src={require("../../images/AddOrder.png")}
                  alt="AddOrder icon"
                ></img>
                <p class="pindex">Easy add or pick up new orders </p>
              </li>
              <li>
                <img
                  src={require("../../images/MyOrderss.png")}
                  alt="MyOrders icon"
                ></img>
                <p class="pindex">User-friendly orders list</p>
              </li>
              <li>
                <img
                  src={require("../../images/settings.png")}
                  alt="Settings icon"
                ></img>
                <p class="pindex">Easily change your account settings</p>
              </li>
            </ul>
          </div>
        </div>
        <footer>
          <div className="footer-container">
            <div className="container-main">
              <ul>
                <li></li>
              </ul>
            </div>
          </div>
        </footer>
      </body>
    );
  }
}

export default LandingPage;
