import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { LOGOUT_USER } from "../../actions/types";
import "../../css/navbar.css";

class Header extends Component {
  constructor() {
    super();
    this.logOut = this.logOut.bind(this);
  }

  logOut = () => {
    localStorage.clear();
  };

  render() {
    let user = this.props.user;
    if (user !== undefined || user !== null) {
      if (user.user !== undefined || user.user !== null) {
        user = user.user;
      }
    }
    return (
      <body className="body-nav">
        <div className="container-header">
          <header className="header">
            <a href="/" className="logo">
              Delivery service
            </a>

            <nav className="nav">
              <a href="#" className="hide-dekstop">
                <img
                  src={require("../../images/menu.png")}
                  alt="toggle menu"
                  className="menu"
                  id="menu"
                ></img>
              </a>

              <ul className="show-dekstop hide-mobile navul" id="nav">
                <li id="exit" className="exit-btn hide-dekstop">
                  <img
                    src={require("../../images/exit.png")}
                    alt="exit menu"
                    width="25px"
                    height="25px"
                  ></img>
                </li>
                <li className="navli">
                  {user.id !== undefined && (
                    <a className="nava" href="/dashboard/myorders">
                      ORDERS
                    </a>
                  )}
                </li>

                <li className="navli">
                  {user.id !== undefined && user.type === "courier" && (
                    <a className="nava" href="/dashboard/choose">
                      CHOOSE ORDERS
                    </a>
                  )}
                </li>
                <li className="navli">
                  {user.id !== undefined && user.type === "courier" && (
                    <a className="nava" href="/map">
                      ORDERS MAP
                    </a>
                  )}
                </li>
                <li className="navli">
                  {user.id !== null && user.id !== undefined && (
                    <a className="nava" href="/user/settings">
                      SETTINGS
                    </a>
                  )}
                </li>

                <li className="navli">
                  {user.id !== undefined && user.type === "admin" && (
                    <a className="nava" href="/admin/users">
                      USERS
                    </a>
                  )}
                </li>
                <li className="navli">
                  {user.id === undefined || user.id === null ? (
                    <a className="nava" href="/login">
                      LOGIN
                    </a>
                  ) : (
                    <a className="nava" href="/" onClick={this.logOut}>
                      LOGOUT
                    </a>
                  )}
                </li>
                <li className="navli">
                  {user.id === undefined && (
                    <a className="nava" href="/register">
                      REGISTER
                    </a>
                  )}
                </li>
              </ul>
            </nav>
          </header>
        </div>
      </body>
    );
  }
}

Header.propTypes = {
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(Header);
