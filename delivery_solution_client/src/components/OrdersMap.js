import React, { Component } from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import { connect } from "react-redux";
import { getOrders } from "../actions/OrderActions";
import PropTypes from "prop-types";

const mapStyles = {
  width: "100%",
  height: "90%",
};

class OrderMap extends Component {
  constructor(props) {
    super(props);
  }

  displayMarkers = () => {
    const { orders } = this.props;

    return orders.map((order) => {
      return (
        <Marker
          key={order.id}
          id={order.id}
          position={{
            lat: order.latitude,
            lng: order.longitude,
          }}
          onClick={() => console.log("You clicked me!")}
        />
      );
    });
  };

  render() {
    return (
      <Map
        google={this.props.google}
        zoom={10}
        style={mapStyles}
        initialCenter={{ lat: 54.898521, lng: 23.903597 }}
      >
        {this.displayMarkers()}
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAkf7NXB_qyo3pfBoJDEJ-d3F44Y6IBUI4",
})(OrderMap);
