import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { getUsers, deleteUser } from "../../actions/AdminActions";
import DeleteIcon from "@material-ui/icons/Delete";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";

class UsersDashboard extends Component {
  componentDidMount() {
    try {
      this.props.getUsers();
    } catch (e) {
      console.log(e);
    }
  }

  onDeleteClick = (id) => {
    this.props.deleteUser(id);
  };

  onEditClick = (id) => {
    this.props.history.push(`/admin/editUser/${id}`);
  };

  render() {
    const useStyles = makeStyles({
      table: {
        minWidth: 650,
      },
      button: {},
    });

    const users = this.props.users.users;
    console.log(users);

    return (
      <TableContainer component={Paper}>
        <Table className={useStyles.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>First Name&nbsp;</TableCell>
              <TableCell>Last Name&nbsp;</TableCell>
              <TableCell>Phone Number&nbsp;</TableCell>
              <TableCell>Type&nbsp;</TableCell>
              <TableCell>Action&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users[0] !== undefined &&
              users !== null &&
              users.map((user) => (
                <TableRow key={user.id}>
                  <TableCell component="th" scope="row">
                    {user.id}
                  </TableCell>
                  <TableCell>{user.email}</TableCell>
                  <TableCell>{user.firstName}</TableCell>
                  <TableCell>{user.lastName}</TableCell>
                  <TableCell>{user.phoneNumber}</TableCell>
                  <TableCell>{user.type}</TableCell>
                  <TableCell>
                    <Button
                      variant="contained"
                      color="primary"
                      className={useStyles.button}
                      startIcon={<SaveIcon />}
                      onClick={this.onEditClick.bind(this, user.id)}
                    ></Button>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={useStyles.button}
                      startIcon={<DeleteIcon />}
                      onClick={this.onDeleteClick.bind(this, user.id)}
                    ></Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

UsersDashboard.propTypes = {
  users: PropTypes.object.isRequired,
  getUsers: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
});

export default connect(mapStateToProps, { getUsers, deleteUser })(
  UsersDashboard
);
