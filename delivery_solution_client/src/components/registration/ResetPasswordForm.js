import React, { Component } from "react";
import PropTypes from "prop-types";
import { resetPassword } from "../../actions/UserActions";
import classnames from "classnames";
import { connect } from "react-redux";
import "../../css/forgotpassword.css";

class ResetPasswordForm extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      errors: {},
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const email = {
      email: this.state.email,
    };
    this.props.resetPassword(email, this.props.history);
  }

  render() {
    const { errors } = this.state;
    return (
      <body>
        <div class="hero">
          <div class="form-box-fpass">
            <div class="button-box-fpass">
              <div id="btnForgot"></div>
              <a href="login.html">
                <button type="button" class="toggle-btn-fpass">
                  Log in
                </button>
              </a>
              <a href="register.html">
                <button type="button" class="toggle-btn-fpass">
                  Register
                </button>
              </a>
            </div>
            <h1 class="hforgot">Forgot your password?</h1>
            <p class="p1">
              Don't worry. Just tell us the email address and we will send an
              email with a new password.
            </p>
            <form
              id="login"
              class="input-group-forgot"
              onSubmit={this.onSubmit}
            >
              <input
                type="email"
                class="input-field-forgot"
                placeholder="Email adress"
                name="email"
                value={this.state.email}
                onChange={this.onChange}
              />
              <p class="pwarning">{errors.email ? errors.email : "\u00A0"}</p>
              <a href="#" class="form-a">
                <button type="submit" class="submit-btn-fpass">
                  Send email
                </button>
              </a>
            </form>
          </div>
        </div>
      </body>
    );
  }
}

ResetPasswordForm.propTypes = {
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors,
  resetPassword: PropTypes.func.isRequired,
});

export default connect(mapStateToProps, { resetPassword })(ResetPasswordForm);
