import React, { Component } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { connect } from "react-redux";
import { changePassword } from "../../actions/UserActions";
import "../../css/changepassword.css";

class ChangePasswordForm extends Component {
  constructor() {
    super();
    this.state = {
      oldPassword: "",
      newPassword: "",
      errors: {},
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const passwordRequest = {
      oldPassword: this.state.oldPassword,
      newPassword: this.state.newPassword,
    };
    console.log(passwordRequest);
    this.props.changePassword(this.props.user.user.id, passwordRequest);
  }

  render() {
    const { errors } = this.props;

    return (
      <body>
        <div class="hero">
          <div class="form-box">
            <h1 className="h1-changepassword">Change password</h1>

            <form
              id="change"
              class="input-group-change"
              onSubmit={this.onSubmit}
            >
              <input
                type="password"
                class="input-field-change"
                placeholder="Old password"
                name="oldPassword"
                value={this.state.oldPassword}
                onChange={this.onChange}
              />
              <p className="warning">
                {errors.oldPassword ? errors.oldPassword : "\u00A0"}
              </p>
              <input
                type="password"
                class="input-field-change"
                placeholder="New password"
                name="newPassword"
                value={this.state.newPassword}
                onChange={this.onChange}
              />
              <p className="warning">
                {errors.newPassword ? errors.newPassword : "\u00A0"}
              </p>
              <a href="#" class="form-a">
                <button type="submit" class="submit-btn-changepassword">
                  Change
                </button>
              </a>
            </form>
          </div>
        </div>
      </body>
    );
  }
}

ChangePasswordForm.propTypes = {
  errors: PropTypes.object.isRequired,
  changePassword: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors,
  user: state.user,
});

export default connect(mapStateToProps, { changePassword })(ChangePasswordForm);
