import React, { Component } from "react";
import PropTypes from "prop-types";
import { loginUser } from "../../actions/LoginActions";
import classnames from "classnames";
import { connect } from "react-redux";
import "../../css/login.css";

class LoginForm extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
      type: "dispatcher",
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const newLogin = {
      email: this.state.email,
      password: this.state.password,
    };
    this.props.loginUser(newLogin, this.props.history);
  }

  render() {
    const { errors } = this.state;
    return (
      <body className="body3">
        <div className="hero">
          <div className="form-box">
            <div className="button-box">
              <div id="btnLogin"></div>
              <a href="/login">
                <button type="button" className="toggle-btn">
                  Log in
                </button>
              </a>
              <a href="/register">
                <button type="button" className="toggle-btn">
                  Register
                </button>
              </a>
            </div>
            <form id="login" className="input-group" onSubmit={this.onSubmit}>
              <div style={{ width: "inherit" }}>
                <input
                  type="email"
                  className="input-field1"
                  placeholder="Email adress"
                  name="email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
                <p className="warning">
                  {errors.email ? errors.email : "\u00A0"}
                </p>
                <input
                  type="password"
                  className="input-field1"
                  placeholder="Password"
                  name="password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                <p className="warning">
                  {errors.password ? errors.password : "\u00A0"}
                </p>
              </div>
              <a href="#">
                <button type="submit" className="submit-btn">
                  Log in
                </button>
              </a>
              <a href="/resetPassword">Forgot Password?</a>
            </form>
          </div>
        </div>
      </body>
    );
  }
}

LoginForm.propTypes = {
  loginUser: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors,
});

export default connect(mapStateToProps, { loginUser })(LoginForm);
