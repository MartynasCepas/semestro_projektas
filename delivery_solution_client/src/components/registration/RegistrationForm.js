import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createDispatcher } from "../../actions/DispatcherActions";
import { createCourier } from "../../actions/CourierActions";
import classnames from "classnames";
import "../../css/register.css";

class RegistrationForm extends Component {
  constructor() {
    super();
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      password: "",
      errors: {},
      type: "dispatcher",
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  //life cycle hooks
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const newUser = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      phoneNumber: this.state.phoneNumber,
      password: this.state.password,
      type: this.state.type,
    };
    if (this.state.type == "courier") {
      this.props.createCourier(newUser, this.props.history);
    } else if (this.state.type == "dispatcher") {
      this.props.createDispatcher(newUser, this.props.history);
    }
  }

  render() {
    const { errors } = this.state;

    return (
      <body className="body2">
        <div className="hero">
          <div className="form-box">
            <div className="button-box">
              <div id="btn"></div>
              <a href="/login">
                <button type="button" className="toggle-btn">
                  Log in
                </button>
              </a>
              <a href="/register">
                <button type="button" className="toggle-btn">
                  Register
                </button>
              </a>
            </div>
            <form
              id="register"
              className="input-group1"
              onSubmit={this.onSubmit}
            >
              <div className="inputDiv">
                <input
                  type="text"
                  className="input-field1"
                  placeholder="First Name"
                  name="firstName"
                  value={this.state.firstName}
                  onChange={this.onChange}
                />
                <p className="warning">
                  {errors.firstName ? errors.firstName : "\u00A0"}
                </p>
                <input
                  type="text"
                  className="input-field1"
                  placeholder="Last Name"
                  name="lastName"
                  value={this.state.lastName}
                  onChange={this.onChange}
                />
                <p className="warning">
                  {errors.lastName ? errors.lastName : "\u00A0"}
                </p>
                <input
                  type="email"
                  className="input-field1"
                  placeholder="Email adress"
                  name="email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
                <p className="warning">
                  {errors.email ? errors.email : "\u00A0"}
                </p>
                <input
                  type="tel"
                  className="input-field1"
                  placeholder="Phone number"
                  name="phoneNumber"
                  value={this.state.phoneNumber}
                  onChange={this.onChange}
                />
                <p className="warning">
                  {errors.phoneNumber ? errors.phoneNumber : "\u00A0"}
                </p>
                <input
                  type="password"
                  className="input-field1"
                  placeholder="Password"
                  name="password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                <p className="warning">
                  {errors.password ? errors.password : "\u00A0"}
                </p>
              </div>
              <p className="prole">Select your role:</p>

              <div className="radioDiv">
                <label htmlFor="rdo1">
                  <input
                    type="radio"
                    id="rdo1"
                    name="type"
                    value="dispatcher"
                    onChange={this.onChange}
                  />
                  <span className="rdo"></span>
                  <span>Dispatcher</span>
                </label>

                <label htmlFor="rdo2">
                  <input
                    type="radio"
                    id="rdo2"
                    name="type"
                    value="courier"
                    onChange={this.onChange}
                  />
                  <span className="rdo"></span>
                  <span>Courier</span>
                </label>
              </div>
              <a className="form-a">
                <button type="submit" className="submit-btn">
                  Register
                </button>
              </a>
            </form>
          </div>
        </div>
      </body>
    );
  }
}

RegistrationForm.propTypes = {
  createDispatcher: PropTypes.func.isRequired,
  createCourier: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors,
});

export default connect(mapStateToProps, { createDispatcher, createCourier })(
  RegistrationForm
);
