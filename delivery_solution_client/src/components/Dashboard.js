import React, { Component } from "react";
import OrderItem from "./order/OrderItem";
import CreateOrderButton from "./order/CreateOrderButton";
import { connect } from "react-redux";
import { getOrders, filterOrders, sortOrders } from "../actions/OrderActions";
import PropTypes from "prop-types";
import Select from "react-select";
import "../css/select.css";

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {};
    this.onSortOptionChangeStatus = this.onSortOptionChangeStatus.bind(this);
    this.onSortOptionChangeCategory = this.onSortOptionChangeCategory.bind(
      this
    );
    this.onSortOptionChangePrice = this.onSortOptionChangePrice.bind(this);
    this.onSortOptionChangeDate = this.onSortOptionChangeDate.bind(this);
  }
  componentDidMount() {
    try {
      const user = this.props.user;
      if (user !== null || user !== undefined) {
        if (user.user !== null || user.user !== undefined) {
          this.props.getOrders(
            user.user.id,
            user.user.type,
            this.props.match.params.type
          );
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  onSortOptionChangeStatus = (selectedOption) => {
    try {
      console.log(selectedOption);
      this.props.filterOrders(
        "filterStatus",
        this.props.user.user.id,
        selectedOption.value
      );
    } catch (e) {
      console.log(e);
    }
  };

  onSortOptionChangeCategory = (selectedOption) => {
    try {
      this.props.filterOrders(
        "filterCategory",
        this.props.user.user.id,
        selectedOption.value
      );
    } catch (e) {
      console.log(e);
    }
  };

  onSortOptionChangePrice = (selectedOption) => {
    try {
      if (selectedOption.value === "asc") {
        this.props.sortOrders(this.props.user.user.id, "sortByPriceAsc");
      } else if (selectedOption.value === "dsc") {
        this.props.sortOrders(this.props.user.user.id, "sortByPriceDsc");
      }
    } catch (e) {
      console.log(e);
    }
  };

  onSortOptionChangeDate = (selectedOption) => {
    try {
      if (selectedOption.value === "asc") {
        this.props.sortOrders(this.props.user.user.id, "sortByCreatedDateAsc");
      } else if (selectedOption.value === "dsc") {
        this.props.sortOrders(this.props.user.user.id, "sortByCreatedDateDsc");
      }
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { orders } = this.props.order;

    let user = this.props.user;
    if (user !== undefined || user !== null) {
      if (user.user !== undefined || user.user !== null) {
        user = user.user;
      }
      if (user !== undefined) {
        user = user.type;
      }
    }
    const type = this.props.match.params.type;

    const filterStyle = {
      display: "inline",
      paddingLeft: "20px",
    };

    const selectStyle = {
      display: "inline-block",
      maxWidth: "150px",
    };

    const optionsSortStatus = [
      { value: "Accepted", label: "ACCEPTED" },
      { value: "Created", label: "CREATED" },
      { value: "Rejected", label: "REJECTED" },
      { value: "Delivered", label: "DELIVERED" },
      { value: "In transit", label: "IN TRANSIT" },
      { value: "Failed to deliver", label: "FAILED TO DELIVER" },
    ];

    const optionsSortCategory = [
      { value: "liquid", label: "LIQUID" },
      { value: "radioactive", label: "RADIOACTIVE" },
      { value: "drugs", label: "DRUGS" },
      { value: "slaves", label: "SLAVES" },
    ];

    const optionsSort = [
      { value: "asc", label: "ASCENDING" },
      { value: "dsc", label: "DESCENDING" },
    ];

    return (
      <div className="orders">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              {type === "choose" && (
                <h1 className="display-4 text-center">Choose Orders</h1>
              )}
              {type === "myorders" && (
                <h1 className="display-4 text-center">
                  {user === "admin" ? "All Orders" : "My Orders"}
                </h1>
              )}
              <br />
              <div>
                {(user === "dispatcher" || user === "admin") && (
                  <CreateOrderButton />
                )}
                <p style={filterStyle}>
                  <strong style={filterStyle}>Filter By: </strong>
                </p>

                <Select
                  options={optionsSortStatus}
                  className="data-list-input"
                  name="sortStatus"
                  placeholder="STATUS"
                  styles={{ display: "inline-block" }}
                  onChange={this.onSortOptionChangeStatus}
                />
                <Select
                  options={optionsSortCategory}
                  className="data-list-input"
                  name="sortStatus"
                  placeholder="CATEGORY"
                  styles={{ display: "inline-block" }}
                  onChange={this.onSortOptionChangeCategory}
                />
                <p style={filterStyle}>
                  <strong style={filterStyle}>Sort By: </strong>
                </p>
                <Select
                  options={optionsSort}
                  className="data-list-input"
                  name="sortStatus"
                  placeholder="PRICE"
                  styles={{ display: "inline-block" }}
                  onChange={this.onSortOptionChangePrice}
                />
                <Select
                  options={optionsSort}
                  className="data-list-input"
                  name="sortStatus"
                  placeholder="DATE"
                  styles={{ display: "inline-block" }}
                  onChange={this.onSortOptionChangeDate}
                />
              </div>
              <br />
              <hr />
              {orders != null &&
                orders.map(
                  (order) =>
                    order.status === "CREATED" &&
                    user === "courier" &&
                    type === "choose" && (
                      <OrderItem key={order.id} order={order} />
                    )
                )}
              {orders != null &&
                orders.map(
                  (order) =>
                    user === "courier" &&
                    type === "myorders" && (
                      <OrderItem key={order.id} order={order} />
                    )
                )}
              {orders != null &&
                orders.map(
                  (order) =>
                    (user === "dispatcher" || user === "admin") && (
                      <OrderItem key={order.id} order={order} />
                    )
                )}
              <br />
              <hr />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  order: PropTypes.object.isRequired,
  getOrders: PropTypes.func.isRequired,
  filterOrders: PropTypes.func.isRequired,
  userId: PropTypes.number.isRequired,
  sortOrders: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  order: state.order,
  user: state.user,
});

export default connect(mapStateToProps, {
  getOrders,
  filterOrders,
  sortOrders,
})(Dashboard);
