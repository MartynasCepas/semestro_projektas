import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateOrder, getOrder } from "../../actions/OrderActions";
import classnames from "classnames";
import "../../css/addorder.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import Select from "react-select";
import Places from "google-places-web";

class UpdateOrder extends Component {
  constructor() {
    super();
    this.state = {
      weight: "",
      dimensions: "",
      origin: "",
      destination: "",
      comment: "",
      errors: {},
      title: "",
      category: "",
      price: 0,
      autocompleteOrigin: {},
      autocompleteDestination: {},
      lng: 0,
      lat: 0,
    };
    Places.apiKey = "AIzaSyAkf7NXB_qyo3pfBoJDEJ-d3F44Y6IBUI4";
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSuggestionClick = this.onSuggestionClick.bind(this);
    this.autocompleteActionHandler = this.autocompleteActionHandler.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
  }

  //life cycle hooks
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    const {
      id,
      weight,
      dimensions,
      origin,
      destination,
      comment,
      errors,
      title,
      category,
      price,
      lng,
      lat,
    } = nextProps.order;

    this.setState({
      id,
      weight,
      dimensions,
      origin,
      destination,
      comment,
      errors,
      title,
      category,
      price,
      lng,
      lat,
    });
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getOrder(id, this.props.history);
  }

  getGeometry = async (placeID) => {
    let geo = {
      lat: 0,
      lng: 0,
    };
    try {
      const response = await Places.details({ placeid: placeID });
      const { status, result } = response;
      geo = {
        lng: result.geometry.location.lng,
        lat: result.geometry.location.lat,
      };
      return geo;
    } catch (error) {
      console.log(error);
      return geo;
    }
  };

  onSuggestionClick = async (field, item) => {
    console.log(item);
    const geo = await this.getGeometry(item.place_id);

    if (field === "origin") {
      this.setState({
        [field]: item.description,
        autocompleteOrigin: {},
        lng: geo.lng,
        lat: geo.lat,
      });
    } else if (field === "destination") {
      this.setState({
        [field]: item.description,
        autocompleteDestination: {},
      });
    }
  };

  autocompleteActionHandler = (fieldName, partialValue) => {
    const radius = 10000;
    const language = "en";
    let partialAddress = `${partialValue}`;

    Places.autocomplete({ input: partialAddress, radius, language })
      .then((results) => {
        console.log(results);
        this.setState({ [fieldName]: results });
      })
      .catch((e) => console.log(e));
  };

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === "origin") {
      this.autocompleteActionHandler("autocompleteOrigin", e.target.value);
    }
    if (e.target.name === "destination") {
      this.autocompleteActionHandler("autocompleteDestination", e.target.value);
    }
    if (e.target.name === "origin" && e.target.value === "") {
      this.setState({ autocompleteOrigin: {} });
    }
    if (e.target.name === "destination" && e.target.value === "") {
      this.setState({ autocompleteDestination: {} });
    }
  }

  onCategoryChange = (selectedOption) => {
    this.setState({ category: selectedOption.value });
  };

  onSubmit(e) {
    e.preventDefault();
    const updateOrder = {
      id: this.state.id,
      weight: this.state.weight,
      dimensions: this.state.dimensions,
      origin: this.state.origin,
      destination: this.state.destination,
      comment: this.state.comment,
      longitude: this.state.lng,
      latitude: this.state.lat,
      title: this.state.title,
      category: this.state.category,
      price: this.state.price,
    };
    this.props.updateOrder(updateOrder, this.props.history);
  }

  render() {
    const { errors } = this.props;

    let itemsOrigin = undefined;
    let itemsDestination = undefined;

    if (
      this.state.autocompleteOrigin !== null ||
      this.state.autocompleteOrigin !== undefined
    ) {
      if (
        this.state.autocompleteOrigin.predictions !== null ||
        this.state.autocompleteOrigin.predictions !== undefined
      ) {
        itemsOrigin = this.state.autocompleteOrigin.predictions;
      }
    }

    const options = [
      { value: "regular", label: "Regular" },
      { value: "liquid", label: "Liquid" },
      { value: "radioactive", label: "Radioactive" },
      { value: "drugs", label: "Illegal Drugs" },
    ];

    const space = "\u00A0";

    return (
      <body className="page">
        <form
          className="form-box-addorder"
          onSubmit={this.onSubmit}
          autocomplete="off"
        >
          <h1 className="hadd">Edit order</h1>

          <input
            type="text"
            placeholder="Title"
            name="title"
            value={this.state.title}
            onChange={this.onChange}
          />

          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">Name of the package</span>
          </div>
          {errors === undefined || errors.title === undefined ? (
            <p className="pwarning-order">{space}</p>
          ) : (
            <p className="pwarning-order">{errors.title}</p>
          )}
          <div
            className="data-list-input"
            style={{
              marginLeft: "92px",
              width: "155px",
              border: "2px solid rgb(61, 161, 255)",
              borderRadius: "4px",
            }}
          >
            <Select
              options={options}
              className="data-list-input"
              placeholder={this.state.category}
              styles={{
                marginLeft: "90px",
                width: "500px",
                border: "2px solid rgb(61, 161, 255)",
                borderRadius: "15px",
                height: "15px",
              }}
              name="category"
              onChange={this.onCategoryChange}
            />
          </div>
          {errors === undefined || errors.category === undefined ? (
            <p className="pwarning-order">{space}</p>
          ) : (
            <p className="pwarning-order">{errors.category}</p>
          )}
          <input
            type="text"
            placeholder="Price"
            name="price"
            value={this.state.price}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">In Euros e.g. 10,25</span>
          </div>
          {errors === undefined || errors.price === undefined ? (
            <p className="pwarning-order">{space}</p>
          ) : (
            <p className="pwarning-order">{errors.price}</p>
          )}
          <input
            type="text"
            placeholder="Origin"
            name="origin"
            value={this.state.origin}
            onChange={this.onChange}
          />
          {/*Autocomplete for origin field */}
          {itemsOrigin !== undefined && (
            <div
              className="list-group"
              style={{ position: "absolute", marginLeft: "70px" }}
            >
              {itemsOrigin.map((item, index) => (
                <button
                  key={index}
                  type="button"
                  className="list-group-item list-group-item-action"
                  onClick={() => this.onSuggestionClick("origin", item)}
                >
                  {item.description}
                </button>
              ))}
            </div>
          )}
          {/*End of Autocomplete for origin field */}
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">
              Sending from e.g. City, Street, postal code
            </span>
          </div>
          {errors === undefined || errors.origin === undefined ? (
            <p className="pwarning-order">{space}</p>
          ) : (
            <p className="pwarning-order">{errors.origin}</p>
          )}
          <input
            type="text"
            placeholder="Destination"
            name="destination"
            value={this.state.destination}
            onChange={this.onChange}
          />
          {/*Autocomplete for Destination field */}
          {itemsDestination !== undefined && (
            <div
              className="list-group"
              style={{ position: "absolute", marginLeft: "70px" }}
            >
              {itemsDestination.map((item, index) => (
                <button
                  key={index}
                  type="button"
                  className="list-group-item list-group-item-action"
                  onClick={() => this.onSuggestionClick("destination", item)}
                >
                  {item.description}
                </button>
              ))}
            </div>
          )}
          {/*End of Autocomplete for Destination field */}
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">
              Sending to e.g. City, Street, postal code
            </span>
          </div>
          {errors === undefined || errors.destination === undefined ? (
            <p className="pwarning-order">{space}</p>
          ) : (
            <p className="pwarning-order">{errors.destination}</p>
          )}
          <input
            type="text"
            placeholder="Weight"
            name="weight"
            value={this.state.weight}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">In kilogrames e.g. 1,3kg</span>
          </div>
          {errors === undefined || errors.weight === undefined ? (
            <p className="pwarning-order">{space}</p>
          ) : (
            <p className="pwarning-order">{errors.weight}</p>
          )}
          <input
            type="text"
            placeholder="Dimensions"
            name="dimensions"
            value={this.state.dimensions}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">In meters e.g. 0,2x0,3x0,2</span>
          </div>
          {errors === undefined || errors.dimensions === undefined ? (
            <p className="pwarning-order">{space}</p>
          ) : (
            <p className="pwarning-order">{errors.dimensions}</p>
          )}
          <input
            type="text"
            placeholder="Comments"
            name="comment"
            value={this.state.comment}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">
              Other important information about delivery
            </span>
          </div>

          <a href="#">
            <button class="add-btn">
              <b>Update</b>
            </button>
          </a>
        </form>
        <script src="js/addorder.js"></script>
      </body>
    );
  }
}

UpdateOrder.propTypes = {
  getOrder: PropTypes.func.isRequired,
  updateOrder: PropTypes.func.isRequired,
  order: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
};

const mapStateToProps = (state) => ({
  order: state.order.order,
  errors: state.errors,
  userId: state.user.user.id,
});

export default connect(mapStateToProps, { updateOrder, getOrder })(UpdateOrder);
