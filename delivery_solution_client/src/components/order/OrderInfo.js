import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getOrder } from "../../actions/OrderActions";
import classnames from "classnames";
import "../../css/orderinfo.css";

class OrderInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      weight: "",
      dimensions: "",
      origin: "",
      destination: "",
      comment: "",
      errors: {},
      title: "",
      category: "",
      price: 0,
      createdAt: "",
      updatedAt: "",
      status: "",
    };
    this.goBack = this.goBack.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    const {
      id,
      weight,
      dimensions,
      origin,
      destination,
      comment,
      errors,
      title,
      category,
      price,
      createdAt,
      updatedAt,
      status,
    } = nextProps.order;

    this.setState({
      id,
      weight,
      dimensions,
      origin,
      destination,
      comment,
      errors,
      title,
      category,
      price,
      createdAt,
      updatedAt,
      status,
    });
  }

  goBack = () => {
    this.props.history.push(`/dashboard/myorders`);
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getOrder(id, this.props.history);
  }

  render() {
    return (
      <div class="pageorderinfo">
        <body>
          <form class="form-box-order-info">
            <h1 class="hinfo">Details</h1>
            <div class="row-info">
              <div class="column">
                <p class="info-text">TITLE:</p>
                <p class="info">{this.state.title}</p>

                <p class="info-text">CREATED_AT:</p>
                <p class="info">{this.state.createdAt}</p>

                <p class="info-text">UPDATED_AT:</p>
                <p class="info">{this.state.updatedAt}</p>

                <p class="info-text">DESTINATION:</p>
                <p class="info">{this.state.destination}</p>

                <p class="info-text">ORIGIN:</p>
                <p class="info">{this.state.origin}</p>

                <p class="info-text">COMMENT:</p>
                <p class="info">{this.state.comment}</p>
              </div>

              <div class="column">
                <p class="info-text">ID:</p>
                <p class="info">{this.state.id}</p>

                <p class="info-text">PRICE:</p>
                <p class="info">{this.state.price}</p>

                <p class="info-text">STATUS:</p>
                <p class="info">{this.state.status}</p>

                <p class="info-text">CATEGORY:</p>
                <p class="info">{this.state.category}</p>

                <p class="info-text">WEIGHT:</p>
                <p class="info">{this.state.weight}</p>

                <p class="info-text">DIMENSIONS:</p>
                <p class="info">{this.state.dimensions}</p>
              </div>
            </div>

            <button class="back-btn" onClick={this.goBack}>
              <b>Back</b>
            </button>
          </form>
        </body>
      </div>
    );
  }
}

OrderInfo.propTypes = {
  getOrder: PropTypes.func.isRequired,
  order: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
};

const mapStateToProps = (state) => ({
  order: state.order.order,
  userId: state.user.user.id,
});

export default connect(mapStateToProps, { getOrder })(OrderInfo);
