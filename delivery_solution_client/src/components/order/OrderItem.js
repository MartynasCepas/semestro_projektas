import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteOrder, changeOrderStatus } from "../../actions/OrderActions";
import { acceptOrder, rejectOrder } from "../../actions/CourierActions";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import InfoIcon from "@material-ui/icons/Info";

class OrderItem extends Component {
  onDeleteClick = (id) => {
    this.props.deleteOrder(id);
  };

  changeStatusHandler = (orderId, newStatus) => {
    if (window.confirm("Are you sure about changing order status?")) {
      if (newStatus === "Accepted") {
        this.props.changeOrderStatus(orderId, newStatus);
        this.props.acceptOrder(this.props.user.id, orderId);
      } else if (newStatus === "Rejected") {
        this.props.rejectOrder(this.props.user.id, orderId);
      } else {
        this.props.changeOrderStatus(orderId, newStatus);
      }
    }
  };

  render() {
    const { order } = this.props;
    const user = this.props.user.type;

    const pstyle = {
      marginBottom: "1rem",
    };

    return (
      <div className="container" style={{ maxWidth: "1450px" }}>
        <div
          className="card card-body bg-light mb-4"
          style={{ border: "1px solid rgb(61, 161, 255)" }}
        >
          <div className="row">
            <div className="col-1">
              <span className="mx-auto">{order.id}</span>
              <Link to={`/orderInfo/${order.id}`}>
                <InfoIcon style={{ marginLeft: "10px" }}></InfoIcon>
              </Link>
            </div>
            <div className="col-lg-3">
              <p style={pstyle}>
                <strong>{order.title}</strong>
              </p>
              <p style={pstyle}>{order.category}</p>
              <p style={pstyle}>$ {order.price}</p>
            </div>

            <div className="col-lg-3">
              <p style={pstyle}>
                <strong>Status:</strong>
              </p>
              <p style={pstyle}>{order.status}</p>
            </div>

            <div className="col-lg-3">
              <p style={pstyle}>
                <strong>Created at:</strong>
              </p>
              <p style={pstyle}>{order.createdAt}</p>
            </div>

            <div className="row-md-4 d-none d-lg-block">
              <ul className="list-group" style={{ flexDirection: "row" }}>
                {(user === "dispatcher" || user === "admin") &&
                  (order.status === "CREATED" || user === "admin") && (
                    <Link to={`/updateOrder/${order.id}`}>
                      <li className="list-group-item update">
                        <EditIcon></EditIcon>
                      </li>
                    </Link>
                  )}
                {(user === "dispatcher" || user === "admin") && (
                  <li
                    className="list-group-item delete"
                    onClick={this.onDeleteClick.bind(this, order.id)}
                  >
                    <DeleteForeverIcon
                      variant="contained"
                      color="secondary"
                    ></DeleteForeverIcon>
                  </li>
                )}
                {user === "courier" && order.status === "CREATED" && (
                  <li
                    className="list-group-item list-group-item-success"
                    onClick={this.changeStatusHandler.bind(
                      this,
                      order.id,
                      "Accepted"
                    )}
                  >
                    <CheckIcon></CheckIcon>
                  </li>
                )}
                {user === "courier" && order.status == "CREATED" && (
                  <li
                    className="list-group-item list-group-item-danger"
                    onClick={this.changeStatusHandler.bind(
                      this,
                      order.id,
                      "Rejected"
                    )}
                    style={{ borderTopWidth: "1px" }}
                  >
                    <ClearIcon></ClearIcon>
                  </li>
                )}
                {user === "courier" && order.status === "ACCEPTED" && (
                  <li
                    className="list-group-item update"
                    onClick={this.changeStatusHandler.bind(
                      this,
                      order.id,
                      "In transit"
                    )}
                    style={{ border: "none" }}
                  >
                    <button type="button" className="btn btn-primary">
                      In Transit
                    </button>
                  </li>
                )}
                {user === "courier" && order.status === "IN_TRANSIT" && (
                  <li
                    className="list-group-item update"
                    onClick={this.changeStatusHandler.bind(
                      this,
                      order.id,
                      "Delivered"
                    )}
                    style={{ padding: "0" }}
                  >
                    <button type="button" className="btn btn-success">
                      Delivered
                    </button>
                  </li>
                )}
                {user === "courier" && order.status === "IN_TRANSIT" && (
                  <li
                    className="list-group-item update"
                    onClick={this.changeStatusHandler.bind(
                      this,
                      order.id,
                      "Failed to deliver"
                    )}
                    style={{ padding: "0" }}
                  >
                    <button type="button" className="btn btn-danger">
                      Failed
                    </button>
                  </li>
                )}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OrderItem.propTypes = {
  deleteOrder: PropTypes.func.isRequired,
  changeOrderStatus: PropTypes.func.isRequired,
  acceptOrder: PropTypes.func.isRequired,
  rejectOrder: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, {
  deleteOrder,
  changeOrderStatus,
  acceptOrder,
  rejectOrder,
})(OrderItem);
