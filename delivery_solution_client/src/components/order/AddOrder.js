import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createOrder } from "../../actions/OrderActions";
import classnames from "classnames";
import Places from "google-places-web";
import "../../css/addorder.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import Select from "react-select";
import { parse } from "@fortawesome/fontawesome-svg-core";

class AddOrder extends Component {
  constructor() {
    super();
    this.state = {
      weight: "",
      dimensions: "",
      origin: "",
      destination: "",
      comment: "",
      errors: {},
      title: "",
      category: "",
      price: "",
      autocompleteOrigin: {},
      autocompleteDestination: {},
      lng: 0,
      lat: 0,
      valid: true,
    };
    Places.apiKey = "AIzaSyAkf7NXB_qyo3pfBoJDEJ-d3F44Y6IBUI4";
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSuggestionClick = this.onSuggestionClick.bind(this);
    this.autocompleteActionHandler = this.autocompleteActionHandler.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.validateData = this.validateData.bind(this);
  }

  validateData() {
    let error = {
      weight: "",
      price: "",
      title: "",
      dimensions: "",
    };
    let validation = true;

    {
      /*Weight Validation*/
    }
    var int = parseInt(this.state.weight, 10);
    if (int <= 0) {
      error.weight = "Weight must be greater than zero";
      validation = false;
    }
    if (int > 50) {
      error.weight = "Weight cannot be above 50 kg";
      validation = false;
    }
    if (isNaN(int)) {
      error.weight = "Weight has to be a number";
      validation = false;
    }
    {
      /*Price Validation*/
    }
    if (this.state.price !== "") {
      int = parseInt(this.state.price, 10);
      if (int > 100) {
        error.price = "Price cannot be greater than 100";
        validation = false;
      }
      if (isNaN(int)) {
        error.price = "Price has to be a number";
        validation = false;
      }
      if (this.state.price.match(/[!@#$%^&*(),?":{}|<>]/)) {
        error.price = "Price has to be made of numbers only";
        validation = false;
      }
    }
    {
      /*Title Validation*/
    }
    if (this.state.title.length < 5) {
      error.title = "Title has to be longer than 5 symbols";
      validation = false;
    }
    if (this.state.title.length > 20) {
      error.title = "Title has to be shorter than 20 symbols";
      validation = false;
    }
    if (
      this.state.title.match(/\d/) ||
      this.state.title.match(/[!@#$%^&*(),.?":{}|<>]/)
    ) {
      error.title = "Title has to be made of letters only";
      validation = false;
    }
    {
      /*Dimensions Validation*/
    }
    var dimValid = /^\d+(\.\d+)?x\d+(\.\d+)?x\d+(\.\d+)?$/.test(
      this.state.dimensions
    );
    if (!dimValid) {
      error.dimensions = "Dimensions has to be in format NUMBERxNUMBERxNUMBER";
      validation = false;
    }
    this.setState({ errors: error, valid: validation });
    return validation;
  }

  //life cycle hooks
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  getGeometry = async (placeID) => {
    let geo = {
      lat: 0,
      lng: 0,
    };
    try {
      const response = await Places.details({ placeid: placeID });
      const { status, result } = response;
      geo = {
        lng: result.geometry.location.lng,
        lat: result.geometry.location.lat,
      };
      return geo;
    } catch (error) {
      console.log(error);
      return geo;
    }
  };

  onSuggestionClick = async (field, item) => {
    console.log(item);
    const geo = await this.getGeometry(item.place_id);

    if (field === "origin") {
      this.setState({
        [field]: item.description,
        autocompleteOrigin: {},
        lng: geo.lng,
        lat: geo.lat,
      });
    } else if (field === "destination") {
      this.setState({
        [field]: item.description,
        autocompleteDestination: {},
      });
    }
  };

  autocompleteActionHandler = (fieldName, partialValue) => {
    const radius = 10000;
    const language = "en";
    let partialAddress = `${partialValue}`;

    Places.autocomplete({ input: partialAddress, radius, language })
      .then((results) => {
        console.log(results);
        this.setState({ [fieldName]: results });
      })
      .catch((e) => console.log(e));
  };

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === "origin") {
      this.autocompleteActionHandler("autocompleteOrigin", e.target.value);
    }
    if (e.target.name === "destination") {
      this.autocompleteActionHandler("autocompleteDestination", e.target.value);
    }
    if (e.target.name === "origin" && e.target.value === "") {
      this.setState({ autocompleteOrigin: {} });
    }
    if (e.target.name === "destination" && e.target.value === "") {
      this.setState({ autocompleteDestination: {} });
    }
  }

  onCategoryChange = (selectedOption) => {
    this.setState({ category: selectedOption.value });
  };

  onSubmit(e) {
    e.preventDefault();
    if (this.validateData()) {
      const newOrder = {
        weight: this.state.weight,
        dimensions: this.state.dimensions,
        origin: this.state.origin,
        destination: this.state.destination,
        comment: this.state.comment,
        longitude: this.state.lng,
        latitude: this.state.lat,
        title: this.state.title,
        category: this.state.category,
        price: parseInt(this.state.price),
      };

      this.props.createOrder(this.props.userId, newOrder, this.props.history);
    }
  }

  render() {
    const { errors } = this.state;

    let itemsOrigin = undefined;
    let itemsDestination = undefined;

    if (
      this.state.autocompleteOrigin !== null ||
      this.state.autocompleteOrigin !== undefined
    ) {
      if (
        this.state.autocompleteOrigin.predictions !== null ||
        this.state.autocompleteOrigin.predictions !== undefined
      ) {
        itemsOrigin = this.state.autocompleteOrigin.predictions;
      }
    }

    if (
      this.state.autocompleteDestination !== null ||
      this.state.autocompleteDestination !== undefined
    ) {
      if (
        this.state.autocompleteDestination.predictions !== null ||
        this.state.autocompleteDestination.predictions !== undefined
      ) {
        itemsDestination = this.state.autocompleteDestination.predictions;
      }
    }

    const options = [
      { value: "regular", label: "Regular" },
      { value: "liquid", label: "Liquid" },
      { value: "radioactive", label: "Radioactive" },
      { value: "drugs", label: "Illegal Drugs" },
    ];

    return (
      <body className="page">
        <form
          className="form-box-addorder"
          onSubmit={this.onSubmit}
          autocomplete="off"
        >
          <h1 className="hadd">Add order</h1>

          <input
            type="text"
            placeholder="Title"
            name="title"
            value={this.state.title}
            onChange={this.onChange}
          />

          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">Name of the package</span>
          </div>
          <p className="pwarning-order">
            {errors.title ? errors.title : "\u00A0"}
          </p>

          <div
            className="data-list-input"
            style={{
              marginLeft: "92px",
              width: "155px",
              border: "2px solid rgb(61, 161, 255)",
              borderRadius: "4px",
            }}
          >
            <Select
              options={options}
              className="data-list-input"
              styles={{
                marginLeft: "90px",
                width: "500px",
                border: "2px solid rgb(61, 161, 255)",
                borderRadius: "15px",
                height: "15px",
              }}
              name="category"
              placeholder="Category"
              onChange={this.onCategoryChange}
            />
          </div>

          <p className="pwarning-order">
            {errors.category ? errors.category : "\u00A0"}
          </p>

          <input
            type="text"
            placeholder="Price"
            name="price"
            value={this.state.price}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">In Euros e.g. 10,25</span>
          </div>
          <p className="pwarning-order">
            {errors.price ? errors.price : "\u00A0"}
          </p>

          <input
            type="text"
            placeholder="Origin"
            name="origin"
            value={this.state.origin}
            onChange={this.onChange}
          />
          {/*Autocomplete for origin field */}
          {itemsOrigin !== undefined && (
            <div
              className="list-group"
              style={{ position: "absolute", marginLeft: "70px" }}
            >
              {itemsOrigin.map((item, index) => (
                <button
                  key={index}
                  type="button"
                  className="list-group-item list-group-item-action"
                  onClick={() => this.onSuggestionClick("origin", item)}
                >
                  {item.description}
                </button>
              ))}
            </div>
          )}
          {/*End of Autocomplete for origin field */}
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">
              Sending from e.g. City, Street, postal code
            </span>
          </div>
          <p className="pwarning-order">
            {errors.origin ? errors.origin : "\u00A0"}
          </p>

          <input
            type="text"
            placeholder="Destination"
            name="destination"
            value={this.state.destination}
            onChange={this.onChange}
          />
          {/*Autocomplete for Destination field */}
          {itemsDestination !== undefined && (
            <div
              className="list-group"
              style={{ position: "absolute", marginLeft: "70px" }}
            >
              {itemsDestination.map((item, index) => (
                <button
                  key={index}
                  type="button"
                  className="list-group-item list-group-item-action"
                  onClick={() => this.onSuggestionClick("destination", item)}
                >
                  {item.description}
                </button>
              ))}
            </div>
          )}
          {/*End of Autocomplete for Destination field */}
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">
              Sending to e.g. City, Street, postal code
            </span>
          </div>
          <p className="pwarning-order">
            {errors.destination ? errors.destination : "\u00A0"}
          </p>

          <input
            type="text"
            placeholder="Weight"
            name="weight"
            value={this.state.weight}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">In kilogrames e.g. 1,3kg</span>
          </div>
          <p className="pwarning-order">
            {errors.weight ? errors.weight : "\u00A0"}
          </p>

          <input
            type="text"
            placeholder="Dimensions"
            name="dimensions"
            value={this.state.dimensions}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">In meters e.g. 0,2x0,3x0,2</span>
          </div>
          <p className="pwarning-order">
            {errors.dimensions ? errors.dimensions : "\u00A0"}
          </p>

          <input
            type="text"
            placeholder="Comments"
            name="comment"
            value={this.state.comment}
            onChange={this.onChange}
          />
          <div className="info-tips">
            <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            <span className="extra-info">
              Other important information about delivery
            </span>
          </div>

          <a href="#">
            <button class="add-btn">
              <b>Add</b>
            </button>
          </a>
        </form>
        <script src="js/addorder.js"></script>
      </body>
    );
  }
}

AddOrder.propTypes = {
  createOrder: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors,
  userId: state.user.user.id,
});

export default connect(mapStateToProps, { createOrder })(AddOrder);
