import React from "react";
import { Link } from "react-router-dom";

const CreateOrderButton = () => {
  return (
    <React.Fragment>
      <Link
        to="/addOrder"
        className="btn btn-lg btn-info"
        style={{ backgroundColor: "rgb(61, 161, 255)", marginLeft: "15px" }}
      >
        Create an Order
      </Link>
    </React.Fragment>
  );
};

export default CreateOrderButton;
