import React, { Component } from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import { connect } from "react-redux";
import { getOrders } from "../actions/OrderActions";
import PropTypes from "prop-types";
import OrdersMap from "./OrdersMap";

class DisplayMap extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    try {
      const user = this.props.user;
      if (user !== null || user !== undefined) {
        if (user.user !== null || user.user !== undefined) {
          this.props.getOrders(user.user.id, user.user.type, "myorders");
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  displayMarkers = () => {
    const { orders } = this.props.order;

    return orders.map((order) => {
      return (
        <Marker
          key={order.id}
          id={order.id}
          position={{
            lat: order.latitude,
            lng: order.longitude,
          }}
          onClick={() => console.log("You clicked me!")}
        />
      );
    });
  };

  render() {
    const { orders } = this.props.order;
    return <OrdersMap orders={orders}></OrdersMap>;
  }
}

DisplayMap.propTypes = {
  order: PropTypes.object.isRequired,
  getOrders: PropTypes.func.isRequired,
  userId: PropTypes.number.isRequired,
};

const mapStateToProps = (state) => ({
  order: state.order,
  user: state.user,
});

export default connect(mapStateToProps, { getOrders })(DisplayMap);
