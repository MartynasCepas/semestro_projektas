import React, { Component } from "react";
import "./App.css";
import Dashboard from "./components/Dashboard";
import Header from "./components/layout/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import AddOrder from "./components/order/AddOrder";
import { Provider } from "react-redux";
import store from "./store";
import UpdateOrder from "./components/order/UpdateOrder";
import RegistrationForm from "./components/registration/RegistrationForm";
import LoginForm from "./components/registration/LoginForm";
import OrderMap from "./components/OrdersMap";
import DisplayMap from "./components/DisplayMap";
import UsersDashboard from "./components/admin/UsersDashboard";
import EditUserForm from "./components/admin/EditUserForm";
import ChangePasswordForm from "./components/registration/ChangePasswordForm";
import ResetPasswordForm from "./components/registration/ResetPasswordForm";
import LandingPage from "./components/layout/LandingPage";
import OrderInfo from "./components/order/OrderInfo";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Header />
            <Route exact path="/dashboard/:type" component={Dashboard} />
            <Route exact path="/addOrder" component={AddOrder} />
            <Route exact path="/updateOrder/:id" component={UpdateOrder} />
            <Route exact path="/register" component={RegistrationForm} />
            <Route exact path="/login" component={LoginForm} />
            <Route exact path="/map" component={DisplayMap} />
            <Route exact path="/admin/users" component={UsersDashboard} />
            <Route exact path="/admin/editUser/:id" component={EditUserForm} />
            <Route exact path="/user/settings" component={ChangePasswordForm} />
            <Route exact path="/resetPassword" component={ResetPasswordForm} />
            <Route exact path="/" component={LandingPage} />
            <Route exact path="/orderInfo/:id" component={OrderInfo} />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
