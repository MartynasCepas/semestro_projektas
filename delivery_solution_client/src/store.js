import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";

const initialState = {
  user: {
    user: {
      id: undefined,
    },
  },
};
const middleware = [thunk];
const persistedState = loadState();

let store;

if (window.navigator.userAgent.includes("Chrome")) {
  store = createStore(
    rootReducer,
    persistedState,
    composeWithDevTools(applyMiddleware(...middleware))
  );
} else {
  store = createStore(
    rootReducer,
    persistedState,
    compose(applyMiddleware(...middleware))
  );
}

function saveState(state) {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("state", serializedState);
  } catch (e) {
    console.log(e);
  }
}

function loadState() {
  try {
    const serializedState = localStorage.getItem("state");
    if (serializedState === null) return initialState;
    return JSON.parse(serializedState);
  } catch (e) {
    console.log(e);
    return initialState;
  }
}

store.subscribe(() => {
  saveState({
    user: store.getState().user,
  });
});

export default store;
