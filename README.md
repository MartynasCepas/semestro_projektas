React front-end aplikacijos paleidimas:
npm install
npm start

Testavimui naudoti Chrome naršyklę

Kad veiktų adreso pasiūlymo funkcija užsakymo formoje,reikalingas šis įjungtas plugin: https://chrome.google.com/webstore/detail/cors-unblock/lfhmikememgdcahcdlaciloancbhjino


# Komandos nariai  

| Name     | Surname  | Email                     |  
| -------- | -------- | ------------------------- |
| Rokas    | Tubutis  | rokas.tubutis@ktu.edu     |
| Rapolas  | Snitkus  | snitkusrapolas@gmail.com  |
| Povilas  | Dirsė    | magahakis@gmail.com       |
| Martynas | Čepas    | martynas.cepas1@gmail.com |

![CPS architecture](https://i.imgur.com/CQDAijh.jpg)

